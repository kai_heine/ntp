/**
 * @file 	NetworkException.cpp
 * @date 	05.06.2016
 * @author 	Christian Juette
 * @version	1.0
 *
 */

#include "NetworkException.hpp"

namespace ntp
{

/**
 * @brief	Constructor of the NetworkException.
 *
 * @param	[in]	error	A string containing a description of the error.
 * @param	[in]	fatal	A bool specifying if the error is fatal (i.e. should
 * 							lead to a stop of the program).
 *
 */
NetworkException::NetworkException(std::string error, bool fatal) noexcept
{
	this->error = error;
	this->fatal = fatal;
}

/**
 *
 * @brief	Returns a description of the Error that has happened.
 *
 * @returns	String describing the error.
 *
 */

std::string NetworkException::toString() const noexcept
{
	return ("NetworkException: " + error);
}

/**
 * @brief	Returns whether the exception should be considered fatal or not.
 * 			A fatal exception should lead to a termination of the program.
 *
 * @returns	A boolean value specifiyng if the exception was fatal or not.
 * 			Return Value	|	Meaning
 * 			---------------	|	---------------------
 * 			true			|	Exception should be considered fatal, terminate the program.
 * 			false			|	Exception is not fatal, carry on.
 *
 */

bool NetworkException::isFatal() const noexcept
{
	return fatal;
}

} // end of namespace ntp
