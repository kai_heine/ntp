/**
 * @file 	FileException.cpp
 * @date 	21.08.2016
 * @author 	Christian Juette
 * @version	1.0
 *
 */

#include "FileException.hpp"

namespace ntp
{

/**
 * @brief	Constructor of the FileException.
 *
 * @param	[in]	error	A string containing a description of the error.
 * @param	[in]	fatal	A bool specifying if the error is fatal (i.e. should
 * 							lead to a stop of the program).
 *
 */
FileException::FileException(std::string error, bool fatal) noexcept
{
	this->error = error;
	this->fatal = fatal;
}

/**
 *
 * @brief	Returns a description of the Error that has happened.
 *
 * @returns	String describing the error.
 *
 */

std::string FileException::toString() const noexcept
{
	return ("FileException: " + error);
}

/**
 * @brief	Returns whether the exception should be considered fatal or not.
 * 			A fatal exception should lead to a termination of the program.
 *
 * @returns	A boolean value specifiyng if the exception was fatal or not.
 * 			Return Value	|	Meaning
 * 			---------------	|	---------------------
 * 			true			|	Exception should be considered fatal, terminate the program.
 * 			false			|	Exception is not fatal, carry on.
 *
 */

bool FileException::isFatal() const noexcept
{
	return fatal;
}

} // end of namespace ntp
