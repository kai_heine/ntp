/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file SharedSystemVariables.hpp
 * @brief
 * @details
 *
 * @date 30.08.2016
 * @author Christian Jütte
 * @version 0.1
 *
 * @copyright Copyright 2016, Christian Jütte
 * 			  All rights reserved.
 */

#ifndef NTP_DATATYPES_SHAREDSYSTEMVARIABLES_HPP_
#define NTP_DATATYPES_SHAREDSYSTEMVARIABLES_HPP_

#include "../Log/Logger.hpp"
#include "../Utility/Constants.hpp"
#include "./Exceptions/FileException.hpp"
#include "./Frame/NTPPacket.hpp"
#include "./Timestamp/LongTimestamp.hpp"
#include "./Timestamp/ShortTimestamp.hpp"
#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace ntp
{

/**
 * @brief	This is a class for sharing system variables between the Client and the Server.
 * 			The actual sharing is done by the KernelInterface, this only contains and manages the data
 *
 * @details	This contains certain variables that need to be shared between the processes. The class provides functions
 * 			to convert those variables into raw bytes, and to read them back in so data can be easily transferred.
 *
 * 			The contents are updated and managed by the synchronizer. The synchronizer also tells the KernelInterface
 * 			to share the new data.
 */
class SharedSystemVariables
{

private:
	///The size of the raw representation, in byte. Must be changed when variables are added.
	static const unsigned char SIZE = 23;

	///Stratum
	unsigned char stratum;

	///Leap Indicator
	ntp::LeapIndicator leapIndicator;

	///Poll-Exponent
	signed char poll;

	///Root Delay
	ShortTimestamp rootDelay;

	///Root Dispersion
	ShortTimestamp rootDispersion;

	///Reference Timestamp
	LongTimestamp referenceTimestamp;

	///Reference ID
	char referenceID[4];

public:
	SharedSystemVariables();
	~SharedSystemVariables();

	//Setter and Getter
	void setStratum(unsigned char stratum);
	unsigned char getStratum();

	void setLeapIndicator(ntp::LeapIndicator leapIndicator);
	ntp::LeapIndicator getLeapIndicator();

	void setPoll(signed char poll);
	signed char getPoll();
	signed char increasePoll(signed char step = 1);
	signed char decreasePoll(signed char step = 1);

	void setRootDelay(ShortTimestamp rootDelay);
	ShortTimestamp getRootDelay();

	void setRootDispersion(ShortTimestamp rootDispersion);
	ShortTimestamp getRootDispersion();

	void setReferenceTimestamp(LongTimestamp referenceTimestamp);
	LongTimestamp getReferenceTimestamp();

	std::string getReferenceID() const;
	void setReferenceID(char const* referenceID);

	//Serialization
	void loadFromRaw(std::vector<unsigned char>& data);
	std::vector<unsigned char> getRawData();
	unsigned char getRawSize();

	std::string toString();

}; //end of class SharedSystemVariables

} //end of namespace ntp

#endif /* NTP_DATATYPES_SHAREDSYSTEMVARIABLES_HPP_ */
