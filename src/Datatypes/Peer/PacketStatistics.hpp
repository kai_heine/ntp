/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file PacketStatistics.hpp
 * @brief Calculates the statistics, if a packet arrives.
 * @details If the Peer responded and the data content is valid, the statistics
 * 			offset, delay and dispersion can be calculated.
 *
 * @date 2016
 * @author Simon Häußler
 * @version 0.1
 *
 * @copyright Copyright (c) 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#ifndef NTP_DATATYPES_ANALYSIS_PACKETSTATISTICS_HPP_
#define NTP_DATATYPES_ANALYSIS_PACKETSTATISTICS_HPP_

#include "../../Utility/Constants.hpp"
#include "../../Utility/KernelInterface.hpp"
#include "../Timestamp/LongTimestamp.hpp"
#include "cmath"

namespace ntp
{

/**
 * @brief 	Calculates the statistics of the arriving packets.
 * @details The statistics of the packets (offset, delay, dispersion) are used to choose the best peer.
 * @date 	27.03.2016
 * @author 	Simon Häußler
 * @version	0.1
 */
class PacketStatistics
{
private:
	//private attributes
	/**
	 * @brief The process time at the arrival of the NTPPacket.
	 * @details The process time of this application starts at zero with this application. So it
	 *  		states a time point since the application start. In this context, it gives the time point
	 *  		when a packet arrives the host since the application start.
	 *
	 */
	std::chrono::steady_clock::time_point processTime;

	/**
	 * @brief The offset value in seconds.
	 * @details It is specified relative to the communicating Peer and so it represents the offset
	 * 			of the system clock of the client relating to the system clock of the time server.
	 * 			The calculation is a difference between the mean of the timestamps.
	 * 			!Attention: The offset value is valid only if the packet needs just as much time
	 * 			to the server as back to the client.
	 */
	double offset;

	/**
	 * @brief The delay value in seconds.
	 * @details It is specified relative to the communicating peer and so it represents the delay
	 * 			of a packet by the propagation delay of the traffic on the used network.
	 * 			The calculation results from the round trip delay (RTT) of the packet.
	 */
	double delay;

	/**
	 * @brief The dispersion value in seconds.
	 * @details It represents the maximum error of the calculated system clock time.
	 * 			The calculation of the dispersion is a sum of:<ul>
	 * 			<li>the in the received packet contained precision,</li>
	 * 			<li>the precision of the system clock and</li>
	 * 			<li>a multiplication of the frequency tolerance PHI and the travel time of the packet.</li>
	 * 			</ul>
	 */
	double dispersion;

public:
	//public functions
	PacketStatistics();
	PacketStatistics(LongTimestamp originTimestamp,
	                 LongTimestamp receiveTimestamp,
	                 LongTimestamp transmitTimestamp,
	                 LongTimestamp destinationTimestamp,
	                 signed char precision);
	~PacketStatistics(void);
	double getOffset(void) const;
	double getDelay(void) const;
	double getDispersion(void) const;
	std::string toString();
	std::chrono::steady_clock::time_point getProcessTime(void) const;

}; //end class PacketStatistics

} //end namespace ntp

#endif /* NTP_DATATYPES_ANALYSIS_PACKETSTATISTICS_HPP_ */
