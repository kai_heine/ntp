/**
 * @file 	Peer.cpp
 * @details This file implements the Peer-Class.
 * @date 	22.06.2016
 * @author 	Christian Jütte
 * @version	1.1
 *
 * @remarks -# Tested: Constructor, compareStats, processMessage, triedReach, isReachable
 * 			-# To be tested further: filter, Destructor, calcSyncDistance
 * 			-# The poll process does not support bursts yet.
 */

#include "Peer.hpp"

namespace ntp
{

/**
* @brief 	Main-Constructor. Initializes the Peer with Default-Values and
* 			the UDP-Address.
*
* @param [in] address The address-structure associated with the peer.
*
*/

Peer::Peer(IPAddress address)
{
	ConfigFile& config = ConfigFile::getInstance();
	//Read from config whether to log stuff
	config.readBool(peerStatLogEnabled, "ClientMode", "ServerLogEnabled", true);
	config.readBool(packetStatLogEnabled, "ClientMode", "PacketLogEnabled", true);

	//Read the burst flags
	config.readBool(burstEnabled, "ClientMode", "BurstEnabled", false);
	config.readBool(initBurstEnabled, "ClientMode", "InitBurstEnabled", false);

	std::string statPath;
	config.readString(statPath, "GeneralSettings", "LogDirectory", "log");
	statPath.append("/");

	//When enabled, create a new file for logging statistics:
	if (packetStatLogEnabled)
	{
		createFileLogger(
		    "packetstat",
		    statPath + "packetstat_" + address.toStringNoPort() + ".log",
		    "Running Time[s]\tOffset[ms]\tDelay[ms]\tDispersion[ms]\tPoll[Log2s]");
	}
	if (peerStatLogEnabled)
	{
		createFileLogger(
		    "peerstat",
		    statPath + "serverstat_" + address.toStringNoPort() + ".log",
		    "Running Time[s]\tOffset[ms]\tDelay[ms]\tDispersion[ms]\tJitter[ms]");
	}

	ntp::Constants& constants = ntp::Constants::getInstance();

	this->address = address;

	// Fill the statisticsQueue with default-Values
	ntp::LongTimestamp defTmstp = ntp::LongTimestamp();
	for (int i = 0; i < 8; i++)
	{
		//push Dummy-Tuple
		statisticsQueue.push_back(ntp::PacketStatistics());
	}

	reach = 0;
	offset = 0;
	delay = 0;
	unreach = 0;
	lastSynced = ntp::KernelInterface::getInstance().getProcessTime();
	lastPoll = lastSynced;
	precision = 0;
	dispersion = constants.getDouble(ntp::ConstantsDouble::maxDisp);
	hostPollExponent = constants.getUChar(ntp::ConstantsUChar::pollMin); //Start with the lowest polling intervals

	jitter = 0.0;
	stratum = 0; //Unspecified stratum at first.
	leapIndicator = LI_NO_WARNING;

	wasPolled = false;

	if (initBurstEnabled)
	{
		//If initBurst is enable, send a burst on the start of the session
		burst = constants.getUChar(ntp::ConstantsUChar::burstCount);
		DEBUG("Peer {} | Sending Burst on start.", address.toString());
	}
	else
	{
		burst = 0;
	}

	//std::cout 	<< "Peer | New peer initialized with address " << address.sin_addr.s_addr
	//			<< " and Port " << address.sin_port << "." << std::endl;
}

/**
* @brief 	Destructor. Deletes statisticsQueue, which calls all Destructors of PacketStatistics.
*
*/

Peer::~Peer()
{
	std::lock_guard<std::mutex> lock(mtx);

	DEBUG("Peer {} | Peer with address {} deleted.", this->getHostname(), address.toString());
}

//Documentation in header
bool Peer::compareStats(const ntp::PacketStatistics& stat1, const ntp::PacketStatistics& stat2)
{
	if (stat1.getDelay() == stat2.getDelay())
	{
		return ntp::KernelInterface::getInstance().calcTimeDifference(stat1.getProcessTime(), stat2.getProcessTime()) > 0;
	}
	else
	{
		return stat1.getDelay() < stat2.getDelay();
	}
}

/**
 *
 * @brief This method implements the clock-filter-algorithm as described on pages 37-39 of RFC5905.
 *
 * @details It first copies all PacketStatistics-Tuples from statisticsQueue to a new, temporary list.
 * 			It then sorts this list by ascending delay. If the tuple with the lowest delay is newer
 * 			than the current Peer-statistics, the calculations are done.
 * 			The Peer-Dispersion and Jitter are calculated. Offset, delay and processTime are updated with
 * 			data from the fittest tuple.
 *
 * 			The method does not implement a popcorn-spike-suppressor as suggested in the RFC.
 *
 * @param	[in]	synchronized	The Synchronizer passes this flag. It specifies whether the client has been synchronized yet or not.
 * 									If the client has not been synchronized, it forces the execution of the filter algorithm.
 *
 * @returns	Returns true if the Peer-values have been updated, because a new packet with a smaller
 * 			delay than the best old packet arrived. The selection algorithm only needs to be
 * 			run if a Peer has changed it's values.
 *
 * 			A return-value of false means that the incoming packet had a larger delay than
 * 			at least one of the last 8 packets arrived. That means that the Peer's statistics
 * 			have not been updated, and thus no new information is available for the system algorithms.
 */
bool Peer::filter(bool synchronized)
{
	bool retVal = false;
	std::lock_guard<std::mutex> lock(mtx);
	DEBUG("Peer {} | Filter: Filter-Algorithm called.", this->getHostname());
	int validTuples = 0; //contains the number of valid tuples in the list (dispersion != MAXDISP)

	/// -# Copy PacketStatistics to temporary List
	std::vector<ntp::PacketStatistics>* tempList =
	    new std::vector<ntp::PacketStatistics>(statisticsQueue.begin(), statisticsQueue.end());

	//Debugging Info:
	//  	std::cout << "Peer | Filter: List was copied to temporary List:" << std::endl;
	// 	std::cout << "Peer | Filter: " << std::setw(5) << "Item" << std::setw(15) << "Delay" << std::setw(15) << "Dispersion" << std::setw(15) << "processTime" << std::endl;
	// 	for(unsigned int i=0; i<tempList->size(); i++){
	// 		std::cout << "Peer | Filter: " << std::setw(5) << i << std::setw(15) << tempList->at(i).getDelay() << std::setw(15) << tempList->at(i).getDispersion() << std::setw(15) << ntp::KernelInterface::getInstance().processTimeToDouble(tempList->at(i).getProcessTime()) << std::endl;
	// 	}

	/// -# Sort by ascending delay
	std::sort(tempList->begin(), tempList->end(), Peer::compareStats); //Should work like this.

	//Debugging Info:
	//  	std::cout << "Peer | Filter: Temporary list was sorted by ascending delay:" << std::endl;
	// 	std::cout << "Peer | Filter: " << std::setw(5) << "Item" << std::setw(15) << "Delay" << std::setw(15) << "Dispersion" << std::setw(15) << "processTime" << std::endl;
	// 	for(unsigned int i=0; i<tempList->size(); i++){
	// 		std::cout << "Peer | Filter: " << std::setw(5) << i << std::setw(15) << tempList->at(i).getDelay() << std::setw(15) << tempList->at(i).getDispersion() << std::setw(15) << ntp::KernelInterface::getInstance().processTimeToDouble(tempList->at(i).getProcessTime()) << std::endl;
	// 	}

	/// -# Only continue, if the packet with the smallest delay is younger than the Peer statistics
	///		or if we haven't synchronized yet.
	//std::cout << "Peer | Filter: Check if filter-Algorithm continues: ";
	if (!synchronized || ntp::KernelInterface::getInstance().calcTimeDifference(tempList->at(0).getProcessTime(), this->lastSynced) > 0)
	{
		retVal = true;
		DEBUG("Peer {} | Updating Peer-Statistics.", this->getHostname());
		//std::cout << "OK" << std::endl;
		/// -# Calculate Peer Dispersion
		/// @f$ \varepsilon = \sum_{i=0}^{i=n-1} \frac{\varepsilon_i}{2^{(i+1)}} @f$

		this->dispersion = 0;
		for (unsigned int i = 0; i < tempList->size(); i++)
		{
			/*
			 *
			 *						i=n-1
			 *						--- 	epsilon_i
			 *		epsilon = 		\ 	    ----------
			 *						/ 		 (i+1)
			 *						--- 	2
			 *						i=0
			 */

			this->dispersion = this->dispersion + (tempList->at(i).getDispersion() / pow(2, (i + 1)));

			if (tempList->at(i).getDispersion() != ntp::Constants::getInstance().getDouble(ntp::ConstantsDouble::maxDisp)) //Check if tuple was a valid one produced by the on-wire-protocol
			{
				validTuples++;
			}
		}

		/// -# Calculate Jitter
		/// @f$ \psi = \frac{1}{(n-1)} \cdot \sqrt{\left[ \sum_{j=1}^{n-1} (\theta_0 - \theta_j)^2 \right]}@f$

		/*
		 *  Let the first stage offset in the sorted list be theta_0; then, for
		 *  the other stages in any order, the jitter is the RMS average
		 *
		 *						  +-----                 -----+^1/2
		 *						  |  n-1                      |
		 *						  |  ---                      |
		 *				  1       |  \                     2  |
		 *	  psi   =  -------- * |  /    (theta_0-theta_j)   |
		 *				(n-1)     |  ---                      |
		 *						  |  j=1                      |
		 *						  +-----                 -----+
		 *
		 *  where n is the number of valid tuples in the filter (n > 1).  In
		 *  order to ensure consistency and avoid divide exceptions in other
		 *  computations, the psi is bounded from below by the system precision
		 *  s.rho expressed in seconds.
		 */

		auto sum = 0;
		for (int i = 1; i < validTuples; i++)
		{
			sum += pow((tempList->at(0).getOffset() - tempList->at(i).getOffset()), 2);
		}

		if (validTuples > 1)
		{
			this->jitter = (1.0 / (validTuples - 1.0)) * sqrt(sum);
		}
		else
		{
			this->jitter = 0;
		}

		if (this->jitter < pow(2, ntp::KernelInterface::getInstance().getSystemPrecision())) //Bound from below
		{
			this->jitter = pow(2, ntp::KernelInterface::getInstance().getSystemPrecision());
		}

		/// -# Add further values to peer
		this->offset = tempList->at(0).getOffset();
		this->delay = tempList->at(0).getDelay();
		this->lastSynced = tempList->at(0).getProcessTime();

		if (peerStatLogEnabled)
		{
			unsigned long RunTime = KernelInterface::getInstance().getProcessTimeMS() / 1000;
			spdlog::get("peerstat")->info("{}\t{:.6f}\t{:.6f}\t{:.6f}\t{:.6f}", RunTime, this->offset * 1000, this->delay * 1000, this->dispersion * 1000, this->jitter * 1000);
		}
	}
	else
	{
		//std::cout << "Don't continue." << std::endl;
		DEBUG("Peer {} | Packet with smallest delay is too old / was already used. LastSynced: {}, Smallest: {}",
		    this->getHostname(), ntp::KernelInterface::getInstance().processTimeToDouble(this->lastSynced),
		    ntp::KernelInterface::getInstance().processTimeToDouble(tempList->at(0).getProcessTime()));
	}

	delete tempList;
	return retVal;
}

/**
 *
 * @brief	This function is called when a new message has arrived from an address that matches the one
 * 			of this Peer.
 *
 * @details This function processes the message by updating the reach-register, setting certain peer variables,
 * 			and adding a new PacketStatistics-tuple to the statisticsQueue. It then calls the filter-function
 * 			to run the clock-filter-algorithm.
 *
 */
void Peer::processMessage(ntp::Message& message)
{
	LongTimestamp destTimestamp = message.getDestTimestamp();
	//std::cout << "Peer | Processing message with destTimestamp: " << destTimestamp.toString() << std::endl;
	ntp::NTPPacket payload = message.getPayload();

	//Switch the leap-indicator. usually this should be done in the synchronizer...
	leapIndicator = payload.getLeapIndicator();

	std::unique_lock<std::mutex> lock(mtx);
	reach |= 1; //Succeeded to reach the server
	unreach = 0;

	//The host poll exponent is set to the minimum of ppoll from the last packet received
	//and hpoll, but not less than MINPOLL or greater than MAXPOLL.
	auto ppoll = payload.getPoll();
	auto minPoll = ntp::Constants::getInstance().getUChar(ntp::ConstantsUChar::pollMin);
	auto maxPoll = ntp::Constants::getInstance().getUChar(ntp::ConstantsUChar::pollMax);
	if (ppoll < hostPollExponent && ppoll >= minPoll && ppoll <= maxPoll)
	{
		hostPollExponent = ppoll;
		DEBUG("Peer {} | Set hostPollExponent to {}", this->getHostname(), hostPollExponent);
	}

	rootDelay = payload.getRootdelay();
	rootDispersion = payload.getRootdispersion();
	stratum = payload.getStratum();
	referenceID = payload.getReferenceId();
	referenceTimestamp = payload.getReferenceTime();

	//Add new tuple to queue and kick last one out.
	statisticsQueue.push_back(ntp::PacketStatistics(payload.getOriginTime(), payload.getReceiveTime(),
	    payload.getTransmitTime(), destTimestamp,
	    payload.getPrecision()));
	statisticsQueue.pop_front();

	if (packetStatLogEnabled)
	{
		PacketStatistics& lastPacket = statisticsQueue.back();
		unsigned long RunTime = KernelInterface::getInstance().getProcessTimeMS() / 1000;
		spdlog::get("packetstat")->info("{}\t{:.6f}\t{:.6f}\t{:.6f}\t{}", RunTime, lastPacket.getOffset() * 1000, lastPacket.getDelay() * 1000, lastPacket.getDispersion() * 1000, ppoll);
	}

	DEBUG("Peer " + this->getHostname() + " | Added new Tuple to statisticsQueue:");
	//std::cout << "Peer | " << std::setw(5) << "Item" << std::setw(8) << "Delay" << std::setw(11) << "Dispersion" << std::setw(11) << "processTime" << std::endl;
	//for(unsigned int i=0; i<statisticsQueue->size(); i++){
	//std::cout << "Peer | " << std::setw(5) << i << std::setw(8) << statisticsQueue->at(i)->getDelay() << std::setw(11) << statisticsQueue->at(i)->getDispersion() << std::endl;
	//}

	lock.unlock();
	///The processTime is not set, since it is updated in the filter-algorithm.
}

/**
 *
 * @brief 	This function calculates and returns the _Peer_ Synchronization Distance @f$ \lambda @f$.
 * 			Notice that this differs from the _Root_ Synchronization Distance.
 *
 * @return	Returns the calculated Peer Synchronization distance @f$ \lambda @f$
 *
 */

double Peer::getSyncDistance()
{

	/**
	 * 	Of particular importance to the mitigation algorithms is the peer
	 *	synchronization distance, which is computed from the delay and
	 *	dispersion.
	 *
	 *	@f[ \lambda = (\delta / 2) + \varepsilon. @f]
	 *
	 *	Note that epsilon and therefore lambda increase at rate PHI. The
	 *	lambda is not a state variable, since lambda is recalculated at each
	 *	use. It is a component of the root synchronization distance used by
	 *	the mitigation algorithms as a metric to evaluate the quality of time
	 *	available from each server.
	 *
	 */

	double peerSyncDist = (this->delay / 2) + this->dispersion;
	//DEBUG("Peer " + this->getHostname() + ": PeerSyncDist = " + std::to_string(peerSyncDist));
	return peerSyncDist;
}

/**
* @brief 	Checks whether a Peer has been reached the last eight times. If this
* 			is not the case, the Peer is considered not reachable.
* @details	Thread-Safe!
*
* @return 	Returns whether Peer is considered reachable (true) or not (false).
*/

bool Peer::isReachable()
{
	std::lock_guard<std::mutex> lock(mtx);
	CHATTY_POLL_PROCESS("Peer {} | Reach-Register: {:b}", this->getHostname(), reach);
	return (reach > 0); //If reach is >0, the Peer is reachable
}

/**
 * @brief	This checks if this Peer is acceptable for synchronization.
 * 			It is implemented according to the fit()-Routine in A.5.2 of RFC 5905, or according to the
 * 			accept()-Routine in A.5.5.3, whichever you like better (they are the same).
 *
 * @remarks	Currently the check for loop-errors only checks if the Peer is synchronized to our
 * 			system peer. The check if it is synchronized to us is not easy to execute,
 * 			since that means that we must know our own public IP. To obtain this
 * 			means using a webservice to get the IP, since even the IP-packets we
 * 			receive from our Peers will go through NAT, and thus their destination
 * 			address will be changed on the way.
 */

bool Peer::isAcceptable()
{
	bool acceptable = true;
	Constants& constants = Constants::getInstance();

	//Check for stratum error
	if (leapIndicator == LI_UNKNOWN)
	{
		acceptable = false;
		DEBUG("Peer {} not acceptable because it is unsynchronized.", hostname);
	}

	if (stratum >= constants.getUChar(ntp::ConstantsUChar::maxStrat))
	{
		acceptable = false;
		DEBUG("Peer {} not acceptable due to stratum error.", hostname);
	}

	//Check for distance error
	// Three different definitions on page 38, 39, and 94 of RFC5905

	//We are using the peer synchronization distance here according to page
	//39 of RFC 5905: "so eventually the synchronization distance exceeds the distance
	//threshold MAXDIST, in which case the association is considered unfit for synchronization.

	//double rootSyncDist = calcRootSyncDistance(peer);
	double syncDist = this->getSyncDistance();
	auto poll = KernelInterface::getInstance().getSystemVariables().getPoll();
	double distanceThreshold = constants.getUChar(ntp::ConstantsUChar::maxDist) + constants.getDouble(ntp::ConstantsDouble::phi) * pow(2, poll);
	if (syncDist > distanceThreshold)
	{
		acceptable = false;
		DEBUG("Peer {} not acceptable due to distance error. SyncDist={}, must be below {}", hostname, syncDist, distanceThreshold);
	}
	//	else
	//	{
	//		DEBUG("Peer " + peer.getHostname() + " is acceptable due to distance. SyncDist=" + std::to_string(syncDist)+ ", must be below " + std::to_string(distanceThreshold));
	//	}

	//Check for loop error. A loop error occurs if
	// a) the Peer is synchronized to us
	// b) the Peer is synchronized to our system Peer
	// we can only check the second condition right now (see remarks)

	if (this->referenceID == ntp::KernelInterface::getInstance().getSystemVariables().getReferenceID())
	{
		acceptable = false;
		DEBUG("Peer {} not acceptable because it is synchronized to our system-peer -> Loop Error.", hostname);
	}

	//Check if peer is reachable
	if (reach == 0)
	{
		acceptable = false;
		DEBUG("Peer {} not acceptable because it isn't reachable.", hostname);
	}

	return acceptable;
}

/**
* @brief 	Is called when a transmission to a Peer has been started.
* 			Updates the reach-register and also inserts a Dummy-Tuple into
* 			the statistics-queue, if the Peer is not considered reachable.
* @details	Thread-Safe!
*/

void Peer::triedReach()
{
	//Update the last Poll time.
	wasPolled = true;
	reach <<= 1; // Shift one bit to the left
	lastPoll = ntp::KernelInterface::getInstance().getProcessTime();

	CHATTY_POLL_PROCESS("Peer {} | Trying to reach the Peer", getHostname());

	std::lock_guard<std::mutex> lock(mtx);

	if (burst == 0)
	{
		//No burst in progress. If a burst should be started will be decided below.

		if (wasPolled && (reach & 0x07) == 0) //If 3 lowest bits of reach are 0...
		{
			statisticsQueue.pop_front();
			ntp::LongTimestamp defTmstp = ntp::LongTimestamp();
			statisticsQueue.push_back(ntp::PacketStatistics()); //... add Dummy-Tuple
			DEBUG("Peer {} | Since the Peer could not be reached the last 3 times, a Dummy-Tuple was added.", getHostname());
		}

		//Poll Process: Update the hostPollExponent
		if (wasPolled && reach == 0)
		{
			//Server is unreachable.

			//If we initialized this Peer by hostname, try to resolve it again in case the IP changed
			//Note: This is not specified in RFC5905
			if (hostname != "")
			{
				DEBUG("Peer {} | Trying to update address by resolving the hostname again.", hostname);
				try
				{
					IPAddress newaddress = ntp::KernelInterface::getInstance().getAddressFromName(hostname);
					address = newaddress;
				}
				catch (ntp::NetworkException& ex)
				{
					LOG_ERROR("Hostname could not be resolved: {}", ex.what());
				}
			}

			//Update unreach-counter, hostPollExponent or start burst
			if (initBurstEnabled && unreach == 0)
			{
				//This was the first time it was unreachable. Send a burst to see if we can reach it.
				burst = Constants::getInstance().getUChar(ntp::ConstantsUChar::burstCount);
				DEBUG("Peer {} | Starting Burst on becoming unreachable.", hostname);
			}
			else if (unreach >= ntp::Constants::getInstance().getUChar(ntp::ConstantsUChar::unreach))
			{
				//Unreach-counter is greater than the specified maximum. Increase the poll exponent
				if (hostPollExponent < ntp::Constants::getInstance().getUChar(ntp::ConstantsUChar::pollMax))
				{
					hostPollExponent++;
					DEBUG("Peer {} | Adding to the hostPollExponent.", getHostname());
				}
			}
			else
			{
				unreach++;
			}
		}
		else
		{
			//Server is reachable.
			//Set poll-interval to system poll intervall
			hostPollExponent = KernelInterface::getInstance().getSystemVariables().getPoll();
			if (burstEnabled && isAcceptable())
			{
				//If bursts are enabled and the server is acceptable for synchronization, start a burst.
				burst = Constants::getInstance().getUChar(ntp::ConstantsUChar::burstCount);
				DEBUG("Peer {} | Starting burst on poll interval.", hostname);
			}
		}
	}
	else
	{
		//Burst is in progress. Simply count down the burst-variable
		burst--;
	}
}

// GET-Functions

double Peer::getJitter()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = jitter;
	return toReturn;
}

double Peer::getOffset()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = offset;
	return toReturn;
}

double Peer::getDelay()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = delay;
	return toReturn;
}

double Peer::getDispersion()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = dispersion;
	return toReturn;
}

ntp::ShortTimestamp Peer::getRootDelay()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = rootDelay;
	return toReturn;
}

ntp::ShortTimestamp Peer::getRootDispersion()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = rootDispersion;
	return toReturn;
}

/**
 * @brief	Determines based on the poll exponent and the lastPoll-time if
 * 			the Peer needs to be polled again.
 *
 * @return	Returns true if the Peer needs to be polled, otherwise false.
 *
 * @note	This might change to a different return type (int) if bursts are added
 *
 */
bool Peer::pollNow()
{
	bool poll = false;
	std::lock_guard<std::mutex> lock(mtx);

	//Time difference (in seconds) to last poll:
	double timeDifference = ntp::KernelInterface::getInstance().calcTimeDifference(ntp::KernelInterface::getInstance().getProcessTime(), lastPoll);

	if (burst == 0)
	{
		//If the difference between now and the lastPoll is greater than or equal to 2^hostPollExponent, then poll.
		CHATTY_POLL_PROCESS("Peer {} | TimeDifference from the last poll: {}, hostPollExponent: {}", getHostname(), timeDifference, hostPollExponent);
		poll = (timeDifference >= pow(2, hostPollExponent));
	}
	else
	{
		//During a burst, poll every burstTime seconds.
		CHATTY_POLL_PROCESS("Peer {} | Peer is in burst.", getHostname());
		poll = timeDifference >= Constants::getInstance().getUChar(ntp::ConstantsUChar::burstTime);
	}

	return (!wasPolled | poll);
}

IPAddress Peer::getAddress()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto toReturn = address;
	return toReturn;
}

unsigned char Peer::getPrecision()
{
	return precision;
}

unsigned char Peer::getStratum()
{
	return stratum;
}

ntp::LeapIndicator Peer::getLeapIndicator()
{
	return leapIndicator;
}

void Peer::setHostname(std::string hostname)
{
	this->hostname = hostname;
}

std::string Peer::getHostname()
{
	return hostname;
}

/**
 * @brief	Returns the point in time when we last received a message from the Peer.
 */
std::chrono::steady_clock::time_point Peer::getLastReceived()
{
	std::lock_guard<std::mutex> lock(mtx);
	auto retVal = lastSynced;
	return retVal;
}

/**
 * @brief	This resets the Peer to it's initial state, loosing all packets
 * 			and calculated statistics and such.
 *
 */
void Peer::reset()
{
	ntp::Constants& constants = ntp::Constants::getInstance();

	statisticsQueue.clear();

	// Fill the statisticsQueue with default-Values
	ntp::LongTimestamp defTmstp = ntp::LongTimestamp();
	for (int i = 0; i < 8; i++)
	{
		//push Dummy-Tuple
		statisticsQueue.push_back(ntp::PacketStatistics());
	}

	reach = 0;

	lastSynced = ntp::KernelInterface::getInstance().getProcessTime();
	lastPoll = lastSynced;

	precision = 0;
	dispersion = constants.getDouble(ntp::ConstantsDouble::maxDisp);
	hostPollExponent = constants.getUChar(ntp::ConstantsUChar::pollMin);
	; //Start with the lowest polling intervals

	jitter = 0.0;
	stratum = 0; //Unspecified stratum at first.

	DEBUG("Peer {} | Reset", getHostname());
}

} // End of namespace ntp
