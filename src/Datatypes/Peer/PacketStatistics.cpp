/**
 * @file PacketStatistics.cpp
 * @see PacketStatistics.hpp
 *
 * @date	2016
 * @author	Simon Häußler
 * @version	0.1
 * @copyright Copyright (c) 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#include "PacketStatistics.hpp"

namespace ntp
{
/**
 * @brief
 * @details It creates a dummy object or a dummy tuple to store
 * 			the statistics of a packet.
 */
PacketStatistics::PacketStatistics(void)
{
	try
	{
		//Setting up the Process Time at the current time
		this->processTime = ntp::KernelInterface::getInstance().getProcessTime();
	}
	catch (ntp::TimeException& ex)
	{
		LOG_ERROR("Can't set the processTime in PacketStatistics::PacketStatistics() due to the following TimeException: {}", ex.toString());
	}

	//Setting up the offset at zero
	this->offset = 0;

	//Setting up the delay at MAXDISP
	this->delay = ntp::Constants::getInstance().getDouble(ntp::ConstantsDouble::maxDisp);

	//Setting up the dispersion at MAXDISP
	this->dispersion = ntp::Constants::getInstance().getDouble(ntp::ConstantsDouble::maxDisp);
}

/**
 * @brief
 * @details This constructor calculates<ul>
 *  		<li>the offset relative to the dedicated Peer,</li>
 *  		<li>the delay relative to the dedicated Peer and</li>
 *  		<li>the dispersion of the system clock</li>
 *  		</ul>
 *  		from the given Timestamps.
 *  		<br>The dispersion statistic represents the maximum error
 *  		due to the frequency tolerance and time since the last NTPPacket
 *  		was sent.
 *  		<br>This is necessary to create an object to compute
 *  		the statistics of the dedicated Peer.
 *
 * @param [orgTimestamp]	Timestamp in NTP Timestamp Format, which represents the
 * 							moment sending the first packet of a session to the Peer.
 * @param [recTimestamp]  	Timestamp in NTP Timestamp Format, which represents the
 * 						  	moment at the Peer, when the Packet arrives.
 * @param [xmtTimestamp] 	Timestamp in NTP Timestamp Format, which represents the
 * 							moment at the Peer, when the Packet was transmitted to the requesting Peer.
 * @param [destTimestamp] 	Timestamp in NTP Timestamp Format, which represents the
 * 							moment retrieving the sent packet from the Peer.
 * @param [precision]		Precision of the system clock of the Peer which sends the packet.
 */
PacketStatistics::PacketStatistics(LongTimestamp orgTimestamp,
                                   LongTimestamp recTimestamp,
                                   LongTimestamp xmtTimestamp,
                                   LongTimestamp destTimestamp,
                                   signed char precision)
{

	//logs that a new tuple has created
	CHATTY_POLL_PROCESS("New statistic: Timestamps: T1: {} T2: {} T3: {} T4: {}", orgTimestamp, recTimestamp, xmtTimestamp, destTimestamp);

	try
	{
		//Setting up the Process Time at the arrival of the packet
		this->processTime = ntp::KernelInterface::getInstance().getProcessTime();
	}
	catch (ntp::TimeException& ex)
	{
		LOG_ERROR("Can't set the processTime in PacketStatistics::PacketStatistics() due to the following TimeException: {}", ex.toString());
	}

	//calculates the offset relative to the peer
	this->offset = 0.5 * ((recTimestamp - orgTimestamp) + (xmtTimestamp - destTimestamp));
	CHATTY_POLL_PROCESS("New statistic: Offset: {}", this->offset);

	//calculates the delay of the peer
	this->delay = (destTimestamp - orgTimestamp) - (xmtTimestamp - recTimestamp);
	CHATTY_POLL_PROCESS("New statistic: Delay: {}", this->delay);

	//The delay can become negative, and thus should be clamped to the system precision (in seconds)
	double systemPrecision = exp2(ntp::KernelInterface::getInstance().getSystemPrecision());
	if (this->delay < systemPrecision)
	{
		this->delay = systemPrecision;
	}

	//calculates the dispersion of the system clock
	double tempTravelTime = static_cast<double>(destTimestamp) - static_cast<double>(orgTimestamp);
	this->dispersion = exp2(precision) + systemPrecision + ntp::Constants::getInstance().getDouble(ntp::ConstantsDouble::phi) * tempTravelTime;
	CHATTY_POLL_PROCESS("New statistic: Dispersion: {}", this->dispersion);
}

/**
 * Destructor, do not do anything.
 */
PacketStatistics::~PacketStatistics(void) {}

/**
 * @brief To get the offset out of the statistic object.
 *
 * @return This function only returns the <b>value</b> of the private offset variable as a double.
 */
double PacketStatistics::getOffset(void) const
{
	return this->offset;
}

/**
 * @brief To get the delay out of the statistic object.
 *
 * @return This function only returns the <b>value</b> of the private delay variable as a double.
 */
double PacketStatistics::getDelay(void) const
{
	return this->delay;
}

/**
 * @brief To get the dispersion out of the statistic object.
 *
 * @return This function only returns the <b>value</b> of the private dispersion variable as a double.
 */
double PacketStatistics::getDispersion(void) const
{
	return this->dispersion;
}

/**
 * @brief To get the time of the process out of the statistic object.
 *
 * @return This function only returns the <b>value</b> of the private processTime variable as a time point.
 */
std::chrono::steady_clock::time_point PacketStatistics::getProcessTime(void) const
{
	return this->processTime;
}

/**
 * @brief Returns a string containing the calculated statistics of the packet.
 *
 * @details The string is in the following format:
 * 			Offset: <i>value</i> | Delay: <i>value</i> | Dispersion: <i>value</i>
 * 			With offset in miliseconds and delay in seconds
 */
std::string PacketStatistics::toString()
{
	return fmt::format("Offset: {} | Delay: {} | Dispersion: {}", offset, delay, dispersion);
}

} //end of namespace ntp
