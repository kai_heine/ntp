/**
 * @file SelectionTuple.cpp
 * @see SelectionTuple.hpp
 *
 * @date 2016
 * @author Simon Häußler
 * @version 0.1
 *
 * @warning ---
 * @copyright Copyright (c) 2016, Simon Häußler.
 * 			  All rights reserved.
 */

#include "SelectionTuple.hpp"

namespace ntp
{

/**
 * @brief
 * @details Initializes a tuple with the results of eight measures.
 * @param [in] peer The Peer, assigned to the tuple.
 * @param [in] type The type of the tuple (-1 = lower edge, 0 = middle, +1 = upper edge)
 */
SelectionTuple::SelectionTuple(ntp::Peer* peer, int type)
{
	//store peer
	this->peer = peer;

	//Log, if the type value is invalid
	if (type < -1 || type > 1)
	{
		this->type = -2; //default
		this->edge = 0;  //default

		WARN("Warning: A tuple gets an invalid type value.");
	}
	else
	{
		//store type value
		this->type = type;

		//measurement for the edge-value in the interval of the peer-offset
		this->edge = peer->getOffset() + type * peer->getSyncDistance();
	}
}

/**
 * Destructor, do not do anything
 */
SelectionTuple::~SelectionTuple(void) {}

/**
 * @brief Function to sort the several tuples.
 * @details It compares the edges of both tuples.
 *
 * @return This function returns <b>true</b> if the edge value of the invoking object
 * 		   is less than the edge value of the given object. In all other cases, it returns <b>false</b>.
 */
bool SelectionTuple::operator<(const ntp::SelectionTuple tuple) const
{
	return (this->edge < tuple.edge);
} // end of function

int ntp::SelectionTuple::getType()
{
	return type;
}

double ntp::SelectionTuple::getEdge()
{
	return edge;
}

ntp::Peer* ntp::SelectionTuple::getPeer()
{
	return peer;
}

} //end of namespace ntp
