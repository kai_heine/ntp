/**
 * @file SurvivorCandidate.cpp
 * @see SurvivorCandidate.hpp
 *
 * @date 19.06.2016
 * @author Christian Jütte
 * @version 0.1
 *
 * @copyright Copyright (c) 2016, Christian Jütte
 * 			  All rights reserved.
 *
 * @brief 	SurvivorCandidates are used in the System Process for the Cluster and
 * 			Combine Algorithm. They aid in finding the system Peer.
 */

#include "SurvivorCandidate.hpp"

namespace ntp
{

/**
 * @brief	Constructs a new survivor-candidate from the given Peer.
 *
 * @param peer The peer to construct the candidate around.
 *
 */
SurvivorCandidate::SurvivorCandidate(ntp::Peer* peer)
{
	this->peer = peer;
	this->jitter = peer->getJitter();
	this->offset = peer->getOffset();

	this->meritFactor = peer->getStratum() * Constants::getInstance().getUChar(ntp::ConstantsUChar::maxDist) + peer->getSyncDistance();
}

/**
 * @brief	Destructor
 *
 */
SurvivorCandidate::~SurvivorCandidate()
{
}

/**
 * @brief	Compares the meritFactors of both tuples.
 *
 * @return	Returns true, if the meritFactor of the other tuple is greater than
 * 			or equal to the one of this object.
 */
bool SurvivorCandidate::operator<(const SurvivorCandidate& tuple) const
{
	return (meritFactor < tuple.meritFactor);
}

ntp::Peer* SurvivorCandidate::getPeer()
{
	return peer;
}

double SurvivorCandidate::getOffset()
{
	return offset;
}

double SurvivorCandidate::getJitter()
{
	return jitter;
}

double SurvivorCandidate::getMeritFactor()
{
	return meritFactor;
}

} //end of namespace ntp
