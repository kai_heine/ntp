/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	IPAddress.hpp
 * @details This Header declares the datatype IPAddress.
 * @author 	christian
 */
#ifndef NTP_DATATYPES_IPADDRESS_HPP_
#define NTP_DATATYPES_IPADDRESS_HPP_

#include <cstdint>
#include <string>

namespace ntp
{

/**
 * @brief	This class provides a platform-independent type for using IP-addresses.
 *
 * The class provides accessors and ways to create strings from the addresses.
 * Currently only IPv4 is implemented.
 *
 */

class IPAddress
{
private:
	uint32_t address; ///< IPv4-address
	uint16_t port;    ///< UDP-port

public:
	IPAddress();
	IPAddress(uint32_t addr, uint16_t prt);
	~IPAddress();
	std::string toStringNoPort() const;
	std::string toString() const;
	uint32_t getAddress() const;
	uint16_t getPort() const;
};

} //end of namespace ntp

#endif /* NTP_DATATYPES_IPADDRESS_HPP_ */
