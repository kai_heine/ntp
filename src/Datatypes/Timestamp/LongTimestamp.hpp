/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	LongTimestamp.hpp
 * @details This Header declares the NTP Timestamp in long format (64bit type).
 * @date 	24.03.2016
 * @author 	silvan
 * @version	0.1
 */

#ifndef NTP_DATATYPES_TIMESTAMP_LONGTIMESTAMP_HPP_
#define NTP_DATATYPES_TIMESTAMP_LONGTIMESTAMP_HPP_

#include "ShortTimestamp.hpp"
#include "Timestamp.hpp"

namespace ntp
{

/**
 * @brief	This class represents 64bit timestamp type
 * @details It supports several operations and is designed to
 * 			be easily used within NTP packets and NTP algorithms.
 */
class LongTimestamp : public Timestamp
{
public:
	LongTimestamp(void);
	LongTimestamp(double time);
	LongTimestamp(unsigned int seconds, unsigned int fraction);
	std::vector<unsigned char> getRaw() const;
	void loadFromRaw(vector_uchar_iter& start) override;
	double operator-(LongTimestamp subtrahend);
};

} //end of namespace ntp

#endif /* NTP_DATATYPES_TIMESTAMP_LONGTIMESTAMP_HPP_ */
