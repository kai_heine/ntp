/**
 * @file 	ShortTimestamp.cpp
 * @details ShortTimestamp type definition (32bit type).
 * @date 	24.03.2016
 * @author 	silvan
 * @version	0.1
 */

#include "ShortTimestamp.hpp"
#include "../../Utility/KernelInterface.hpp"
#include <cmath>
#include <iostream>
#include <string>

namespace ntp
{

/**
 * @brief	Simple constructor. Initializes timestamp with zero.
 *
 */
ShortTimestamp::ShortTimestamp() : Timestamp()
{
}

/**
 * @brief	see Timestamp()
 */
ShortTimestamp::ShortTimestamp(double time) : Timestamp(time)
{
}

/**
 * @brief	Uses seconds and fraction to initialize the timestamp.
 *
 */
ShortTimestamp::ShortTimestamp(unsigned int seconds, unsigned int fraction) : Timestamp(seconds, fraction)
{
}

/**
 * @brief 	Returns a vector of bytes suitable for adding to a UDP frame.
 * @details	Internally, this method casts the class values for seconds and fraction
 * 			to unsigned 16-bit values. It should take the least significant bits of
 * 			the seconds field an the most significant bits of the fraction field.
 */
std::vector<unsigned char> ShortTimestamp::getRaw() const
{
	register unsigned int fraction = this->fraction >> 16;
	register unsigned int seconds = this->seconds >> 0;

	uint16_t buffer[2];
	std::vector<unsigned char> data(sizeof(buffer));
	buffer[0] = KernelInterface::hostToNetworkOrder(static_cast<uint16_t>(seconds));
	buffer[1] = KernelInterface::hostToNetworkOrder(static_cast<uint16_t>(fraction));

	uint8_t* array = reinterpret_cast<unsigned char*>(buffer);
	for (unsigned int i = 0; i < sizeof(buffer); i++)
	{
		data[i] = array[i];
	}

	return data;
}

/**
 * @brief	creates a timestamp from raw data of type std::vector<unsigned char>
 * @details	This method takes the start position in a vector of unsigned char.
 * 			It will increment the iterator by 4. If start can not be incremented by four,
 * 			this method may throw an exception when trying to dereference start, depending
 * 			on the implementation of std::vector.
 */
void ShortTimestamp::loadFromRaw(vector_uchar_iter& start)
{
	// load seconds
	uint16_t seconds;
	seconds = ((uint16_t)*start) << 8; // byte 1
	start++;
	seconds += (uint16_t)*start; // byte 2
	start++;

	this->seconds = static_cast<unsigned int>(seconds);

	// load fraction
	uint16_t fraction;
	fraction = static_cast<uint16_t>(*start); // byte 3
	fraction <<= 8;
	start++;
	fraction += static_cast<uint16_t>(*start); // byte 4
	start++;

	this->fraction = static_cast<unsigned int>(fraction) << 16;
}

/**
 * @brief	Subtracts two timestamps and returns the result as double.
 *
 * @details	This function has to be used since the actual subtraction should be done
 * 			in 64-Bit arithmetic, as recommended by RFC5905.
 * 			Only later is the value converted to double.
 *
 */

double ShortTimestamp::operator-(ShortTimestamp subtrahend)
{
	uint64_t thisTimestamp = seconds;
	thisTimestamp = thisTimestamp << 32;
	thisTimestamp += fraction;

	uint64_t subtr = subtrahend.getSeconds();
	subtr = subtr << 32;
	subtr += subtrahend.getFraction();

	double result = ((int64_t)(thisTimestamp - subtr)) * NTPLONG_FRAC_RESOLUTION;

	return result;
}

} //end of namespace ntp
