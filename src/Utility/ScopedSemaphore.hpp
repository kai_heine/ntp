/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	ScopedSemaphore.hpp
 * @details This Header declares the ScopedSemaphore Class.
 * @date 	29.11.2017
 * @author 	Silvan König
 * @version	1.0
 */

#ifndef NTP_UTILITY_SCOPEDSEMAPHORE_HPP_
#define NTP_UTILITY_SCOPEDSEMAPHORE_HPP_

/**
 * @brief	std::lock_guard for boost::asio::semaphores
 * @details	This class should work like the lock_guard from the C++ standard library but with
 * 			semaphores from the boost::asio library. It should work with all semaphore-types
 * 			which provide a wait() and a post() method.
 *
 * 			Once instantiated with a semaphore object, its wait() method will be called. On
 * 			destruction of the ScopedSemaphore object, the semaphores post() method will be called.
 */
template <class sem_type>
class ScopedSemaphore
{

private:
	sem_type& sem;

public:
	inline ScopedSemaphore<sem_type>(sem_type& sem) : sem(sem)
	{
		sem.wait();
	}

	inline ~ScopedSemaphore<sem_type>()
	{
		sem.post();
	}
};

#endif /* NTP_UTILITY_SCOPEDSEMAPHORE_HPP_ */
