/**
 * @file 	KernelInterface.cpp
 * @details This file implements common parts of the KernelInterface-Class
 * @date 	25.11.2017
 * @author 	Christian Jütte, Silvan König
 * @version	2.0
 *
 * @remarks Good info at:
 * 			- https://www.freebsd.org/cgi/man.cgi?query=ntp_adjtime&sektion=2
 * 			- http://www.gnu.org/software/libc/manual/html_node/High-Accuracy-Clock.html
 */

#include "KernelInterface.hpp"
#include "../Log/Logger.hpp"
#include "../NTP.hpp"
#include "ConfigFile.hpp"
#include "ScopedSemaphore.hpp"

#include <boost/asio/steady_timer.hpp>

namespace ntp
{

using namespace boost::interprocess;
using namespace boost::asio;

/**
 * 
 * @brief 	Constructor
 *
 * @details Loads systemPrecision from Constants using the key "SYSTEM_PRECISION".
 *          It also initializes the the shared memory, the semaphore and the networking
 *          system.
 *
 *          The implementation differs depending on the configuration (which platform / using boost or not).
 *
 */

KernelInterface::KernelInterface()
    : shm{open_or_create, SHM_NAME, read_write},
      shmSemaphore{open_or_create, SHM_NAME, 1}
{
	systemPrecision = Constants::getInstance().getChar(ConstantsChar::systemPrecision);
	startTime = std::chrono::steady_clock::now();
	shm.truncate(OFFSET_SYSTEMVARIABLES + sizeof(systemVariables));
	sharedSystemVariablesRegion = mapped_region{shm, read_write, OFFSET_SYSTEMVARIABLES, sizeof(systemVariables)};
	sharedServerProcessId = mapped_region{shm, read_write, OFFSET_SERVER_PID, PID_SIZE};
	sharedClientProcessId = mapped_region{shm, read_write, OFFSET_CLIENT_PID, PID_SIZE};
	sharedMemInitialized = true;

	DEFAULT_LOG("KernelInterface initialized.");
}

/**
 *
 * @brief 	Private Standard-Destructor. Closes the socket and deletes the PID from shared memory.
 *
 */
KernelInterface::~KernelInterface()
{
	DEFAULT_LOG("Cleaning up... ");

	if (socketInitialized)
	{
		closeSocket();
	}

	if (sharedMemInitialized)
	{
		//Delete our pid from the shared memory object.
		//We leave the reference timestamp there, since this is actually the last time we synced!
		DEBUG("Clearing PID from shared memory.");
		deletePIDFromSharedMem();
	}

	DEFAULT_LOG("Cleanup done.");
}

/**
 *
 * @brief	Returns the Singleton-Object of the class.
 *
 * @details It just returns the internal object.
 */
KernelInterface& KernelInterface::getInstance()
{
	return instance;
}

/**
 *
 * @brief	Function to get the current process-Time,
 * 			that is the time the program has been running.
 *
 * @details	This can be used to compare times of packet arrival
 * 			or last updates, when the system time has been
 * 			adjusted.
 *
 * @returns	The current time the process has been running as
 * 			a std::chrono::time_point as specified in \<chrono>.
 *
 */
std::chrono::steady_clock::time_point KernelInterface::getProcessTime()
{
	return std::chrono::steady_clock::now();
}

/**
 *
 * @brief	Function to get the current process-Time,
 * 			that is the time the program has been running, in miliseconds
 *
 * @details	This should only be used for logging purposes because of the limited resolution
 *
 * @returns	The current time the process has been running in miliseconds
 *
 */
unsigned long KernelInterface::getProcessTimeMS()
{
	double time = processTimeToDouble(getProcessTime());
	return static_cast<unsigned long>(std::abs(time) * 1000 + 0.5);
}

/**
 * @brief	Calculates the difference in time between two std::chrono::time_points.
 * 			The calculation performed is tp1-tp2.
 *
 * @return	A double (signed!) that contains the difference between the time_points in seconds.
 *
 */
double KernelInterface::calcTimeDifference(std::chrono::steady_clock::time_point tp1, std::chrono::steady_clock::time_point tp2)
{
	return (std::chrono::duration_cast<std::chrono::duration<double>>(tp1 - tp2)).count();
}

/** @brief	When a processTime-Timestamp is supplied, the function converts it to a double
 * 			that represents the number of seconds since the start of the program.
 *
 *
 */
double KernelInterface::processTimeToDouble(std::chrono::steady_clock::time_point tp)
{
	return calcTimeDifference(tp, startTime);
}

void KernelInterface::deletePIDFromSharedMem()
{

	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	switch (NTP::getMode())
	{

	case NTP::AppMode::MODE_CLIENT:
		*static_cast<pid_t*>(sharedClientProcessId.get_address()) = 0;
		break;

	case NTP::AppMode::MODE_SERVER:
		*static_cast<pid_t*>(sharedServerProcessId.get_address()) = 0;
		break;
	}
}

void KernelInterface::initSocket()
{
	if (this->socketInitialized)
	{
		throw NetworkException("Socket already initialized", true);
	}

	DEBUG("Initializing socket.");

	std::uint16_t port = 0;

	switch (NTP::getMode())
	{
	case NTP::MODE_SERVER: port = Constants::getInstance().getUShort(ConstantsUShort::ntpPort); break;
	case NTP::MODE_CLIENT: port = 0; break;
	}

	try
	{
		udpsocket = std::make_unique<ip::udp::socket>(ioservice, ip::udp::endpoint{ip::udp::v4(), port});
		DEBUG("Socket bound to port {}.", port);
		socketInitialized = true;
	}
	catch (boost::system::system_error& e)
	{
		std::string error = "Port could not be bound. Ending the application.";
		LOG_ERROR("{} {}", error, e.what());
		throw(NetworkException(error, true));
	}
}

void KernelInterface::closeSocket()
{
	try
	{
		udpsocket->close();
		udpsocket.reset();
		socketInitialized = false;
	}
	catch (boost::system::system_error& e)
	{
		LOG_ERROR("Error: unable to close socket: {}", e.what());
	}
	// todo: might throw another exception asio::error::operation_aborted or similar
}

std::uint32_t KernelInterface::hostToNetworkOrder(std::uint32_t value)
{
	return detail::socket_ops::host_to_network_long(value);
}

std::uint16_t KernelInterface::hostToNetworkOrder(std::uint16_t value)
{
	return detail::socket_ops::host_to_network_short(value);
}

void KernelInterface::transmitPacket(Message& msg)
{
	using detail::socket_ops::network_to_host_long;

	if (!this->socketInitialized)
	{
		throw NetworkException("Socket uninitialized", true);
	}

	IPAddress const& msgAddress = msg.getPeerAddress();

	try
	{
		using namespace std::chrono;

		ip::udp::endpoint ep{ip::address_v4{network_to_host_long(msgAddress.getAddress())}, msgAddress.getPort()};
		udpsocket->send_to(buffer(msg.getPayload().createFrame()), ep);
	}
	catch (boost::system::system_error& e)
	{
		std::string errorString = fmt::format("Failed to transmit message to the Peer with address {}", msgAddress.toString());
		LOG_ERROR("{}. {}", errorString, e.what());
		throw(NetworkException(errorString, false));
	}
}

std::shared_ptr<Message> KernelInterface::receivePacket()
{
	using detail::socket_ops::host_to_network_long;

	if (!this->socketInitialized)
	{
		LOG_ERROR("Socket uninitialized");
		throw NetworkException("Socket uninitialized", true);
	}

	LongTimestamp recvtime;
	std::vector<std::uint8_t> receiveBuffer(2048);

	unsigned int timeout = Constants::getInstance().getUInt(ConstantsUInt::recvTimeout);
	steady_timer timer(ioservice);
	ioservice.restart();

	ip::udp::endpoint senderEndpoint;
	udpsocket->async_receive_from(buffer(receiveBuffer), senderEndpoint,
	    [&, this](auto error, auto bytesTransferred) {
		    if (!error)
		    {
			    recvtime = getSocketTimestamp();
			    timer.cancel();
			    receiveBuffer.resize(bytesTransferred);
		    }
	    });

	timer.expires_after(std::chrono::seconds(timeout));
	timer.async_wait([this](auto error) {
		if (!error)
		{
			udpsocket->cancel();
			throw TimeoutException("Timeout");
		}
	});

	ioservice.run();

	auto ip_ad = senderEndpoint.address().to_v4().to_ulong();

	IPAddress ip{host_to_network_long(ip_ad), senderEndpoint.port()};

	return std::make_shared<Message>(ip, receiveBuffer, recvtime);
}

IPAddress KernelInterface::getAddressFromName(std::string hostname)
{
	using detail::socket_ops::host_to_network_long;

	ip::udp::endpoint ep;

	try
	{
		ip::udp::resolver resolver(this->ioservice);
		ip::udp::resolver::query query(ip::udp::v4(), hostname, "");

		ep = *resolver.resolve(query);
	}
	catch (boost::system::system_error& e)
	{
		throw NetworkException{fmt::format("Hostname {} could not be resolved.", hostname), false};
	}

	if (ep.port() == 0)
	{
		ep.port(123);
	}

	ip::address ip = ep.address();

	uint32_t ipAsInt = host_to_network_long(ip.to_v4().to_ulong());

	return IPAddress(ipAsInt, ep.port());
}

bool KernelInterface::updateSystemVariables(SharedSystemVariables newSystemVariables)
{
	CHATTY_UPDATE_SYSTEM_VARIABLES("Updating system variables: {}", newSystemVariables.toString());

	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	SharedSystemVariables* svar = static_cast<SharedSystemVariables*>(this->sharedSystemVariablesRegion.get_address());

	if (svar == nullptr)
	{
		return false;
	}

	*svar = newSystemVariables;
	this->systemVariables = newSystemVariables;

	return true;
}

SharedSystemVariables KernelInterface::getSystemVariables()
{
	return *static_cast<SharedSystemVariables*>(this->sharedSystemVariablesRegion.get_address());
}

} // namespace ntp
