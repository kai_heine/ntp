/*
* ----------------------------------------------------------------------------
* Copyright 2018 Ostfalia University of Applied Sciences, Germany
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* ----------------------------------------------------------------------------
*/

/**
 * @file 	KernelInterface_Linux.cpp
 * @details This file implements the parts of the KernelInterface-Class, that are used when compiling for native Linux.
 * @date 	05.06.2016
 * @author 	Christian Jütte, Silvan König
 * @version	2.0
 *
 * @remarks Good info at:
 * 			- https://www.freebsd.org/cgi/man.cgi?query=ntp_adjtime&sektion=2
 * 			- http://www.gnu.org/software/libc/manual/html_node/High-Accuracy-Clock.html
 */

#include "KernelInterface.hpp"

#ifdef NTP_LINUX

#include "../NTP.hpp"
#include "ScopedSemaphore.hpp"
#include <cmath>
#include <linux/errqueue.h>
#include <linux/net_tstamp.h>
#include <linux/sockios.h>
#include <sys/timex.h>

#ifdef NTP_MUSL_SUPPORT

#define ntp_adjtime adjtimex

extern "C" int ntp_gettime(struct ntptimeval* ntv)
{
	timex tx{};

	int result = ntp_adjtime(&tx);

	ntv->time = tx.time;
	ntv->maxerror = tx.maxerror;
	ntv->esterror = tx.esterror;

	return result;
}

#endif

#define HIER DEBUG("HIER");

namespace ntp
{

/* free functions */

timeval ntpToUnixTimestamp(const Timestamp& tmstmp)
{
	uint32_t unixtime = tmstmp.getSeconds();
	if (unixtime >= Timestamp::UNIX_NTP_TIME_OFFSET)
	{
		unixtime -= Timestamp::UNIX_NTP_TIME_OFFSET;
	}
	else
	{
		unixtime += UINT32_MAX - Timestamp::UNIX_NTP_TIME_OFFSET;
	}

	timeval t;
	time_t sec = unixtime;
	time_t usec = static_cast<time_t>(Timestamp::NTPLONG_FRAC_RESOLUTION * tmstmp.getFraction() * 1.0e6);

	t.tv_sec = static_cast<time_t>(sec);
	t.tv_usec = static_cast<suseconds_t>(usec);

	return t;
}

LongTimestamp unixToNTPTimestamp(const timeval& time)
{
	LongTimestamp tmstmp(static_cast<unsigned int>(time.tv_sec) + Timestamp::UNIX_NTP_TIME_OFFSET, static_cast<unsigned int>(1.0e-6 * time.tv_usec * UINT32_MAX));
	return tmstmp;
}

/* member functions */

bool KernelInterface::doProcessesHaveSameName(pid_t processOne, pid_t processTwo)
{
	bool result = false;
	std::string nameOne = "";
	std::string nameTwo = "";

	std::string fileOne = "/proc/" + std::to_string(processOne) + "/comm";
	std::string fileTwo = "/proc/" + std::to_string(processTwo) + "/comm";

	std::ifstream input(fileOne);
	input >> nameOne;
	//DEBUG(nameOne);
	input.close();

	input.open(fileTwo);
	input >> nameTwo;
	//DEBUG(nameTwo);
	input.close();

	if (nameOne == nameTwo)
	{
		result = true;
	}

	return result;
}

ntp::LongTimestamp KernelInterface::getTime()
{
	timex tmx{}; //Struct to set resolution to microseconds
	ntptimeval ntpTimeval;
	ntp::LongTimestamp tmstmp(0);

	//Set resolution of the system clock to microseconds
	tmx.modes = ADJ_MICRO;
	ntp_adjtime(&tmx);
	//Precision is returned in microseconds, must be converted to log2 seconds
	systemPrecision = static_cast<signed char>(log2(((double)tmx.precision) / 1000000));

	//Get the time
	if (ntp_gettime(&ntpTimeval) >= 0)
	{
		//Getting time successful, construct the Timestamp
		tmstmp = unixToNTPTimestamp(ntpTimeval.time);
		//std::cout << "getTime: s: " << ntpTimeval.time.tv_sec << " us: " << ntpTimeval.time.tv_usec << std::endl;
	}
	else
	{
		//Getting time unsuccessful
		throw ntp::TimeException(fmt::format("Error in KernelInterface::getTime: The current system time could not be fetched: {}", std::strerror(errno)));
	}

	return tmstmp;
}

ntp::LongTimestamp KernelInterface::getSocketTimestamp()
{
	using namespace std::chrono;
	auto now = getTime();

	if (!udpsocket)
	{
		return now;
	}

	timeval socketTimestamp{};
	if (ioctl(udpsocket->native_handle(), SIOCGSTAMP, &socketTimestamp) < 0)
	{
		DEBUG("Could not get socket timestamp: {}", std::strerror(errno));
		return now;
	}

	return unixToNTPTimestamp(socketTimestamp);
}

void KernelInterface::adjustTime(double offset)
{
	timex tmx{}; //Time-struct

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the statusWord in KernelInterface::adjustTime(): {}", std::strerror(errno)));
	}

	tmx.offset = offset * 1000000.0; //Set offset

	// http://man7.org/linux/man-pages/man2/adjtimex.2.html if mod_nano is not set, adjtime adds 4 onto the constant
	//RFC 1589, p.29. Set to 0 as default for fast convergence
	tmx.constant = static_cast<long>(ntp::Constants::getInstance().getUInt(ntp::ConstantsUInt::pllTimeConstant)) - 4;

	//Select resolution (microseconds) and offset-adjustment
	tmx.modes = ADJ_OFFSET | ADJ_MICRO | ADJ_TIMECONST | ADJ_MAXERROR | ADJ_ESTERROR | ADJ_STATUS;

	tmx.status = (tmx.status | STA_PLL) & ~STA_UNSYNC; //Activate PLL, clock is synced

	tmx.esterror = static_cast<double>(systemVariables.getRootDispersion()) * 1e6;
	tmx.maxerror = static_cast<double>(systemVariables.getRootDispersion()) + static_cast<double>(systemVariables.getRootDelay()) / 2;

	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error in KernelInterface::adjustTime: The system time could not be adjusted: {}", std::strerror(errno)));
	}

	//DEBUG("Status-Flag: " + std::to_string(tmx.status));
	//DEBUG("Modes: " + std::to_string(tmx.modes));
	//Precision is returned in microseconds, must be converted to log2 seconds
	systemPrecision = static_cast<signed char>(log2(((double)tmx.precision) / 1000000));

	//std::cout << "kInterfaceMaxerr: " << tmx.maxerror << " esterr: " << tmx.esterror << " [us]" <<std::endl;
}

void KernelInterface::setFrequencyOffset(double frequencyOffset)
{
	timex tmx{}; //Time-struct

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the statusWord in KernelInterface::adjustTime(): {}", std::strerror(errno)));
	}

	tmx.modes = tmx.modes | ADJ_FREQUENCY | ADJ_MICRO; //Adjust Frequency

	int integerPart = static_cast<int>(frequencyOffset) << 16;
	unsigned short fractionalPart = static_cast<unsigned short>((UINT16_MAX * 1.0) * (frequencyOffset - integerPart));

	tmx.freq = (integerPart & 0xFFFF0000) + fractionalPart;

	//std::cout << "kInterface(calc) freqOffset (hex): " << std::hex << tmx.freq << std::endl;

	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting frequency unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error in KernelInterface::setFrequencyOffset: The frequency offset could not be set: {}", std::strerror(errno)));
	}

	//std::cout << "kInterfacePPS freq: " << std::fixed << static_cast<double>(tmx.ppsfreq) / 65536.0 << " PPM" <<std::endl;
}

double KernelInterface::getFrequencyOffset()
{
	timex tmx{}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the frequencyOffset in KernelInterface::getFrequencyOffset(): {}", std::strerror(errno)));
	}

	//Calculate frequency offset
	auto integerPart = static_cast<int>(tmx.freq >> 16);
	double frac = (tmx.freq & 0xFFFF) / 65536.0;

	return (integerPart * 1.0 + frac);
}

void KernelInterface::setTime(double offset)
{
	timespec t{};
	clock_gettime(CLOCK_REALTIME, &t);
	std::int64_t ns = offset * 1000000000 + t.tv_nsec;

	if (ns < 0)
	{
		t.tv_nsec = (ns % 1000000000) + 1000000000;
		t.tv_sec -= 1;
	}
	else
	{
		t.tv_nsec = ns % 1000000000;
	}
	t.tv_sec += ns / 1000000000;

	if (clock_settime(CLOCK_REALTIME, &t) < 0)
	{
		LOG_ERROR("Could not set the system time: {}", std::strerror(errno));
	}
}

signed char KernelInterface::getSystemPrecision()
{
	return systemPrecision;
}

double KernelInterface::getJitter()
{
	timex tmx{}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the Jitter: {}", std::strerror(errno)));
	}

	//Convert jitter from microseconds to double
	double jitter = (tmx.jitter * 1.0) / 1000000.0;
	return jitter;
}

double KernelInterface::getOffset()
{
	timex tmx{}; //Time-struct
	tmx.modes |= ADJ_MICRO;

	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the Offset: {}", std::strerror(errno)));
	}

	//Convert offset from microseconds to double
	double offset = (tmx.offset * 1.0) / 1000000.0;
	return offset;
}

void KernelInterface::insertLeapSecond()
{
	timex tmx{}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the statusWord in KernelInterface::insertLeapSecond(): {}", std::strerror(errno)));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = (tmx.status | STA_INS) & ~STA_DEL & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error inserting leap second in KernelInterface::insertLeapSecond(): {}", std::strerror(errno)));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_61SEC);
	updateSystemVariables(sysVar);

	DEBUG("Leap second inserted.");
}

void KernelInterface::deleteLeapSecond()
{
	timex tmx{}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the statusWord in KernelInterface::deleteLeapSecond(): {}", std::strerror(errno)));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = (tmx.status | STA_DEL) & ~STA_INS & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error deleting leap second in KernelInterface::deleteLeapSecond(): {}", std::strerror(errno)));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_59SEC);
	updateSystemVariables(sysVar);

	DEBUG("leap second deleted.");
}

void KernelInterface::resetLeapSecond()
{
	timex tmx{}; //Time-struct

	tmx.modes = 0;
	// Get current status-word
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error obtaining the statusWord in KernelInterface::resetLeapSecond(): {}", std::strerror(errno)));
	}

	tmx.modes = tmx.modes | MOD_STATUS;
	tmx.status = tmx.status & ~STA_DEL & ~STA_INS & ~STA_UNSYNC;
	//Insert leap second
	if (ntp_adjtime(&tmx) < 0)
	{
		//Adjusting time unsuccessful, throw exception
		throw ntp::TimeException(fmt::format("Error resetting leap second in KernelInterface::resetLeapSecond(): {}", std::strerror(errno)));
	}

	//Also set the leapIndicator in shared system variables
	SharedSystemVariables sysVar = getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_NO_WARNING);
	updateSystemVariables(sysVar);

	DEBUG("Leap second reset.");
}

ntp::LeapIndicator KernelInterface::getLeapSecond()
{
	//Old implementation, not in use because of problems with STA_UNSYNC
	// ---------------------------------------------
	/*	ntptimeval timeVal = {0};
	int retVal = 0;
	if((retVal = ntp_gettime(&timeVal)) < 0)
	{
		//Getting time unsuccessful, throw exception
		std::stringstream errorString("Error getting the time in KernelInterface::getLeapSecond(): ");
		errorString << std::strerror(errno);
		throw(ntp::TimeException(errorString.str()));
	}
	switch(retVal)
	{
	case TIME_WAIT:
		//Clear the Leap-Indicator flag
		resetLeapSecond();
	case TIME_OK:
	case TIME_OOP:
		INFO("Keine Warning");
		return ntp::LeapIndicator::LI_NO_WARNING;
	case TIME_INS:
		INFO("Ins");
		return ntp::LeapIndicator::LI_61SEC;
	case TIME_DEL:
		INFO("Del");
		return ntp::LeapIndicator::LI_59SEC;

	case TIME_ERROR:
	default:
		INFO("Err");
		return ntp::LeapIndicator::LI_UNKNOWN;
	}*/
	// ---------------------------------------------
	// Instead, shared system variables are used now.

	return getSystemVariables().getLeapIndicator();
}

bool KernelInterface::consoleInputAvailable()
{
	timeval tv;
	fd_set fds;
	tv.tv_sec = 0;
	tv.tv_usec = 0;
	FD_ZERO(&fds);
	FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
	select(STDIN_FILENO + 1, &fds, NULL, NULL, &tv);
	return FD_ISSET(STDIN_FILENO, &fds);
}

bool KernelInterface::programHasNecessaryRights()
{
	/*
	 * Perform Capability checks for the following Capablities:
	 * 		+ CAP_SYS_TIME
	 * 			- set the system clock (settimeofday, stime, adjtimex)
	 * 		+ CAP_IPC_LOCK
	 * 			- Lock memory (mlock, mmap, shmctl)
	 * 		+ CAP_NET_BIND_SERVICE
	 * 			- Bind a socket to Internet domain privileged ports (port numbers less than 1024)
	 * 		+ (maybe) CAP_NET_ADMIN
	 * 			- perform various network related operations
	 * 		+ (maybe) CAP_NET_BROADCAST
	 * 			- make some socket broadcasts and multicasts
	 * 		+ (maybe) CAP_NET_RAW
	 * 			- use RAW and PACKET sockets
	 *
	 */

	bool result = true;

	if (geteuid() != 0)
	{
		result = false;
	}

	return result;
}

bool KernelInterface::isOtherProcessRunning()
{
	ScopedSemaphore<boost::interprocess::named_semaphore> _s(this->shmSemaphore);

	bool result = false;
	pid_t myId = getpid();

	pid_t sharedId = 0;

	switch (ntp::NTP::getMode())
	{
	case ntp::NTP::AppMode::MODE_CLIENT:
		sharedId = *static_cast<pid_t*>(this->sharedClientProcessId.get_address());
		break;
	case ntp::NTP::AppMode::MODE_SERVER:
		sharedId = *static_cast<pid_t*>(this->sharedServerProcessId.get_address());
		break;
	}

	if (sharedId == myId)
	{
		result = false; // We are already part of that semaphore
	}
	else
	{
		if (doProcessesHaveSameName(sharedId, myId))
		{
			// process was started twice
			result = true;
		}
		else
		{
			switch (ntp::NTP::getMode())
			{
			case ntp::NTP::AppMode::MODE_CLIENT:
				*static_cast<pid_t*>(sharedClientProcessId.get_address()) = myId;
				break;
			case ntp::NTP::AppMode::MODE_SERVER:
				*static_cast<pid_t*>(sharedServerProcessId.get_address()) = myId;
				break;
			}

			result = false;
		}
	}

	return result;
}

} //End of namespace ntp

#endif
