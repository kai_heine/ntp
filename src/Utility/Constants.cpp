/**
 * @file    Constants.cpp
 * @details Constants type definition
 * @date    05.06.2016
 * @author  Thorben Kompa
 * @version 0.3
 */

#include "Constants.hpp"
#include "../Log/Logger.hpp"

namespace ntp
{

/**
* @brief      Creates a Constants
*/
Constants::Constants(void)
{
	adjLimit = 0.128;
	burstCount = 8;
	burstTime = 2;
	maxDisp = 16;
	minDisp = 0.01;
	maxStrat = 16;
	ntpPort = 123;
	phi = 15e-6;
	pollMax = 10;
	pollMin = 6;
	recvTimeout = 2;
	pllTimeConstant = 2;
	systemPrecision = -18;
	unreach = 24;
	maxDist = 1;

	std::ifstream inStream;
	std::ofstream outStream;

	std::string row;
	std::string constant;
	std::string value;
	int firstSpace;
	int secondSpace;
	const int distanceSpace = 3;

	bool tryRead = true;

	inStream.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	try
	{
		inStream.open(NTP_CONSTANTS_FILE_PATH, std::ios::in);
	}
	catch (std::exception& inEx)
	{
		DEFAULT_LOG("Could not open constants file: {}", inEx.what());

		outStream.exceptions(std::ofstream::badbit | std::ofstream::failbit);

		try
		{
			DEFAULT_LOG("Trying to create new file {}", NTP_CONSTANTS_FILE_PATH);
			outStream.open(NTP_CONSTANTS_FILE_PATH, std::ios::out);

			outStream << "ADJ_LIMIT = 0.128 \n"
			          << "BCOUNT = 8 \n"
			          << "BTIME = 2 \n"
			          << "MAXDISP = 16 \n"
			          << "MINDISP = 0.005 \n"
			          << "MAXSTRAT = 16 \n"
			          << "MAXDIST = 1 \n"
			          << "NTP_PORT = 123 \n"
			          << "PHI = 15e-6 \n"
			          << "PLL_TIME_CONSTANT = 2 \n"
			          << "POLL_MAX = 17 \n"
			          << "POLL_MIN = 4 \n"
			          << "RECV_TIMEOUT = 100 \n"
			          << "SYSTEM_PRECISION = -18 \n"
			          << "UNREACH = 24 \n";

			outStream.close();

			inStream.open(NTP_CONSTANTS_FILE_PATH, std::ios::in);
		}
		catch (std::exception& outEx)
		{
			DEFAULT_LOG("Could not create new file {}, using default values: {}", NTP_CONSTANTS_FILE_PATH, outEx.what());
			tryRead = false;
		}
	}

	if (tryRead)
	{
		try
		{
			if (inStream.is_open())
			{
				// peek is used to determine whether there is another character to be read by getline()
				while (!inStream.eof() && inStream.peek() != std::char_traits<char>::eof())
				{
					getline(inStream, row);

					firstSpace = row.find(" ", 0);

					secondSpace = firstSpace + distanceSpace;

					constant = row.substr(0, firstSpace);

					value = row.substr(secondSpace, std::string::npos);

					if (constant == NTP_CONSTANTS_STR_ADJLIMIT)
					{
						adjLimit = std::stod(value, nullptr);
					}
					else if (constant == NTP_CONSTANTS_STR_BCOUNT)
					{
						burstCount = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_BTIME)
					{
						burstTime = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_MAXDISP)
					{
						maxDisp = std::stod(value, nullptr);
					}
					else if (constant == NTP_CONSTANTS_STR_MINDISP)
					{
						minDisp = std::stod(value, nullptr);
					}
					else if (constant == NTP_CONSTANTS_STR_MAXSTRAT)
					{
						maxStrat = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_MAXDIST)
					{
						maxDist = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_UNREACH)
					{
						unreach = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_NTPPORT)
					{
						ntpPort = static_cast<short>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_PHI)
					{
						phi = std::stod(value, nullptr);
					}
					else if (constant == NTP_CONSTANTS_STR_POLLMAX)
					{
						pollMax = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_POLLMIN)
					{
						pollMin = static_cast<unsigned char>(std::stoi(value, nullptr, 10));
					}
					else if (constant == NTP_CONSTANTS_STR_PLLTIMECONST)
					{
						pllTimeConstant = std::stoul(value, nullptr, 10);
					}
					else if (constant == NTP_CONSTANTS_STR_RCVTMO)
					{
						recvTimeout = std::stoul(value, nullptr, 10);
					}
					else if (constant == NTP_CONSTANTS_STR_SYSPREC)
					{
						systemPrecision = static_cast<char>(std::stoi(value, nullptr, 10));
					}
				}
				inStream.close();
			}
			else
			{
				DEFAULT_LOG("Could not read file {}. Using default values.", NTP_CONSTANTS_FILE_PATH);
			}
		}
		catch (std::exception& e)
		{
			DEFAULT_LOG("Could not read file {}, using default values: {}", NTP_CONSTANTS_FILE_PATH, e.what());
		}
	}
	DEFAULT_LOG("Constants initialized.");
}

/**
* @brief      Destructs a Constants
*/
Constants::~Constants(void)
{
}

/**
* @brief      Returns the Constants instance
*
* @return     Constants&
*/
Constants& Constants::getInstance(void)
{
	return instance;
}

double Constants::getDouble(ConstantsDouble param)
{
	switch (param)
	{
	case ConstantsDouble::adjLimit: return adjLimit;
	case ConstantsDouble::maxDisp: return maxDisp;
	case ConstantsDouble::minDisp: return minDisp;
	case ConstantsDouble::phi: return phi;
	default: break;
	}
	return 0;
}

unsigned char Constants::getUChar(ConstantsUChar param)
{
	switch (param)
	{
	case ConstantsUChar::burstCount: return burstCount;
	case ConstantsUChar::burstTime: return burstTime;
	case ConstantsUChar::maxDist: return maxDist;
	case ConstantsUChar::maxStrat: return maxStrat;
	case ConstantsUChar::unreach: return unreach;
	case ConstantsUChar::pollMax: return pollMax;
	case ConstantsUChar::pollMin: return pollMin;
	default: break;
	}
	return 0;
}

unsigned char Constants::getChar(ConstantsChar param)
{
	switch (param)
	{
	case ConstantsChar::systemPrecision: return systemPrecision;
	default: break;
	}
	return 0;
}

unsigned int Constants::getUInt(ConstantsUInt param)
{
	switch (param)
	{
	case ConstantsUInt::recvTimeout: return recvTimeout;
	case ConstantsUInt::pllTimeConstant: return pllTimeConstant;
	default: break;
	}
	return 0;
}

unsigned short Constants::getUShort(ConstantsUShort param)
{
	switch (param)
	{
	case ConstantsUShort::ntpPort: return ntpPort;
	default: break;
	}
	return 0;
}

} // end of namespace ntp
