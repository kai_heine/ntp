/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
* @file      Constants.hpp
* @brief     Constants header
*
* @version   0.3
* @date      05.06.2016
* @author    Thorben Kompa
* @copyright ---
*
* @details   ---
*/

#ifndef NTP_CONSTANTS_CONSTANTS_HPP_
#define NTP_CONSTANTS_CONSTANTS_HPP_

#define NTP_CONSTANTS_FILE_PATH "config/ntp/Konstanten.txt"
#define NTP_CONSTANTS_STR_ADJLIMIT "ADJ_LIMIT"
#define NTP_CONSTANTS_STR_BCOUNT "BCOUNT"
#define NTP_CONSTANTS_STR_BTIME "BTIME"
#define NTP_CONSTANTS_STR_MAXDISP "MAXDISP"
#define NTP_CONSTANTS_STR_MINDISP "MINDISP"
#define NTP_CONSTANTS_STR_PHI "PHI"
#define NTP_CONSTANTS_STR_MAXSTRAT "MAXSTRAT"
#define NTP_CONSTANTS_STR_MAXDIST "MAXDIST"
#define NTP_CONSTANTS_STR_POLLMAX "POLL_MAX"
#define NTP_CONSTANTS_STR_POLLMIN "POLL_MIN"
#define NTP_CONSTANTS_STR_UNREACH "UNREACH"
#define NTP_CONSTANTS_STR_SYSPREC "SYSTEM_PRECISION"
#define NTP_CONSTANTS_STR_RCVTMO "RECV_TIMEOUT"
#define NTP_CONSTANTS_STR_PLLTIMECONST "PLL_TIME_CONSTANT"
#define NTP_CONSTANTS_STR_NTPPORT "NTP_PORT"

#include <fstream>
#include <iostream>
#include <map>
#include <string>

namespace ntp
{

enum class ConstantsDouble
{
	adjLimit,
	maxDisp,
	minDisp,
	phi,
};

enum class ConstantsUChar
{
	maxStrat,
	maxDist,
	burstCount,
	burstTime,
	unreach,
	pollMax,
	pollMin,
};

enum class ConstantsChar
{
	systemPrecision,
};

enum class ConstantsUInt
{
	recvTimeout,
	pllTimeConstant,
};

enum class ConstantsUShort
{
	ntpPort,
};

class Constants
{
private:
	static Constants instance;

	std::map<std::string, int> mapping;

	double adjLimit;
	unsigned char burstCount;
	unsigned char burstTime;
	double maxDisp;
	double minDisp;
	unsigned char maxStrat;
	unsigned char maxDist;
	unsigned short ntpPort;
	double phi;
	unsigned char pollMax;
	unsigned char pollMin;
	unsigned int recvTimeout;
	unsigned int pllTimeConstant;
	char systemPrecision;
	unsigned char unreach;

	Constants(void);
	Constants(const Constants&) = delete;
	~Constants(void);

public:
	static Constants& getInstance(void);

	double getDouble(ConstantsDouble param);
	unsigned char getUChar(ConstantsUChar param);
	unsigned char getChar(ConstantsChar param);
	unsigned int getUInt(ConstantsUInt param);
	unsigned short getUShort(ConstantsUShort param);
};

} // namespace ntp

#endif
