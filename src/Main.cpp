/*
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Main.cpp
 * @details This File implements the main class with the main function which
 * will execute at the beginning of the program.
 * @date 	31.01.2017
 * @author 	Simon Häußler
 * @version	0.1
 */

#include "NTP.hpp"

/**
 * @brief	Main function
 * @details	This function is called, if the program starts it only creates
 * an object of the NTP class to start the NTP-communication
 */

int main()
{
	// logging:
	// basic messages: console + file
	// statistics: (packet, server, system) file

	try
	{
		ntp::NTP ntp;
		ntp.run();
	}
	catch (std::exception& e)
	{
		DEFAULT_LOG(e.what());
	}
	return 0;
}
