/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	ConsoleInterface.hpp
 * @details This Header declares the ConsoleInterface Class for Console I/O.
 * @date 	11.12.2016
 * @author 	Christian Jütte
 * @version	0.1
 */

#ifndef NTP_THREAD_CONSOLEINTERFACE_HPP_
#define NTP_THREAD_CONSOLEINTERFACE_HPP_

#include "../NTP.hpp"
#include "../Utility/KernelInterface.hpp"
#include "../Utility/target_specific_thread.hpp"
#include "Receiver.hpp"
#include "Synchronizer.hpp"
#include "Transmitter.hpp"
#include <chrono>
#include <iostream>

namespace ntp
{

/**
 * @brief	This defines the ConsoleInterface class, which is used to control the application using the console.
 *
 * For documentation of the currently available commands please see page \ref commands.
 *
 * @remarks Further commands might be added in the processCommands-function. Ideas include a 'getsystemstatus'-command to get
 * 			current statistics of the system, a list of the Peers with statistics etc.
 */

class ConsoleInterface
{

private:
	///Object of the KernelInterface
	ntp::KernelInterface& kernelInterface;

	///Object of the current Receiver
	ntp::Receiver* receiver;

	///Object of the current Transmitter
	ntp::Transmitter* transmitter;

	///Object of the current Synchronizer
	ntp::Synchronizer* synchronizer;

	///Mode of the application (Client/Server)
	ntp::NTP::AppMode mode;

	///
	void processConsoleInput();

	void processCommand(std::string& input);

	std::string toLowerCase(std::string& str);

	void outputLine(std::string str);

public:
	/// Constructor for server-mode
	ConsoleInterface(ntp::Receiver& receiver);

	/// Constructor for client-mode
	ConsoleInterface(ntp::Receiver& receiver, ntp::Transmitter& transmitter, ntp::Synchronizer& synchronizer);

	/// The copy-constructor is explicitly deleted since we don't want to copy the ConsoleInterface
	ConsoleInterface(const ConsoleInterface&) = delete;
	~ConsoleInterface();
	void operator()();

}; //End of class ConsoleInterface

} //End of namespace ntp

#endif /* NTP_THREAD_CONSOLEINTERFACE_HPP_ */
