/**
 * @file 	Synchronizer.cpp
 * @details This file implements the Synchronizer-Class.
 * @date 	22.06.2016
 * @author 	Christian Jütte
 * @version	0.1
 */

#include "Synchronizer.hpp"
#include "../NTP.hpp"

namespace ntp
{

/**
 * @brief	Constructor. Adds peers to the peerList.
 *
 * @throws	Throws an ntp::FileException if no Peers could be added to the PeerList.
 *
 *
 */

Synchronizer::Synchronizer(ntp::Receiver& receiver, ntp::Transmitter& transmitter)
    : kernelInterface(ntp::KernelInterface::getInstance()),
      receiver(receiver), transmitter(transmitter), systemOffset(0), systemJitter(0.0),
      selectionJitter(0.0)
{
	//Initialize config-variables
	ConfigFile& config = ConfigFile::getInstance();
	if (!config.readInt(MIN_CANDIDATES, "ClientMode", "MinimumCandidates", 1))
		WARN("Failed to read MinimumCandidates from config-file. Setting to default.");

	if (!config.readInt(MIN_SURVIVORS, "ClientMode", "MinimumSurvivors", 3))
		WARN("Failed to read MinimumSurvivors from config-file. Setting to default.");

	if (!config.readBool(PANIC_ENABLED, "ClientMode", "PanicEnabled", true))
		WARN("Failed to read PanicEnabled from config-file. Setting to default.");

	if (!config.readInt(PANIC_THRESHOLD, "ClientMode", "PanicThreshold", 1000))
		WARN("Failed to read PanicThreshold from config-file. Setting to default.");

	if (!config.readDouble(STEP_THRESHOLD, "ClientMode", "StepThreshold", 0.125))
		WARN("Failed to read StepThreshold from config-file. Setting to default.");

	if (!loadPeersFromConfig())
	{
		throw FileException("No Peers were specified in the config-File.", false);
	}
	lastSync = kernelInterface.getProcessTime();

	if (!config.readBool(systemStatLogEnabled, "ClientMode", "SystemLogEnabled", true))
		WARN("Failed to read SystemLogEnabled from config-file. Setting to default.");

	if (systemStatLogEnabled)
	{
		std::string statPath;
		if (!config.readString(statPath, "GeneralSettings", "LogDirectory", "log"))
			WARN("Failed to read LogDirectory from config-file. Setting to default.");

		statPath.append("/");

		createFileLogger(
		    "systemstat",
		    statPath + "stat_system.log",
		    "Running Time[s]\tOffset[ms]\tSystemJitter[ms]\tSelectionJitter[ms]\tFrequencyOffset[PPM]");
	}
}

/**
 * @brief	Destructor. Cleans up the peerList
 *
 */

Synchronizer::~Synchronizer()
{
	//Clean up the peerList
	for_each(peerList.begin(), peerList.end(), [](Peer* peer) {
		delete peer;
	});

	//Setting client to unsynchronized when we exit
	SharedSystemVariables sysVar = kernelInterface.getSystemVariables();
	sysVar.setLeapIndicator(ntp::LeapIndicator::LI_UNKNOWN);
	kernelInterface.updateSystemVariables(sysVar);
}

/**
 * @brief	Function to run the Synchronizer. This repeatedly runs the syncProcess.
 *
 */
void Synchronizer::operator()()
{
	while (!(NTP::getExceptionPtr()))
	{
		try
		{
			syncProcess();
		}
		catch (...)
		{
			LOG_ERROR("Error in Synchronizer.");
			//Set global exception-pointer
			NTP::setExceptionPtr(std::current_exception());
		}
	} //End of while
}

/**
 * @brief	This basically implements the poll and clock_adjust process as specified in
 * 			RFC 5905.
 * 			Every second it checks whether packets have to be transmitted to Peers
 * 			and if so, it does. It also fetches all packets that have been received
 * 			and performs the system process, eventually adjusting or setting the clock.
 *
 */

void Synchronizer::syncProcess()
{
	lastPoll = kernelInterface.getProcessTime();
	bool runSelect = false; //Determines whether to run the select algorithm
	bool receivedMessages = false;

	///Send packets to all Peers
	///For all Peers check if a transmit should be done.
	///Burst Mode is currently not supported.
	std::for_each(peerList.begin(), peerList.end(), [this](Peer* peer) {
		if (peer->pollNow())
		{
			transmitter.addTransmitTask(*peer);
			peer->triedReach();
		}
	});

	//Get all recently received messages and do the filter algorithms
	while (receiver.isMessageAvailable())
	{
		receivedMessages = true;
		Message currentMessage = receiver.getOldestMessage();
		Peer* peer;

		//DEBUG("Looking for Peer " + currentMessage.getPeerAddress().toString());
		if (getPeerByAddress(currentMessage.getPeerAddress(), &peer))
		{
			//Address matched a Peer
			//Ask Peer to process it and filter
			peer->processMessage(currentMessage);
			runSelect |= peer->filter(synchronized);
		}
		else
		{
			//Address did not match a Peer.
			//Blacklist the address or something? Currently we only put out a warning. This shouldn't be an issue for a client so much..
			WARN("The Address of a received message did not match a Peer.. Received address: {}", currentMessage.getPeerAddress().toString());
		}
	}

	if (syncEnabled && receivedMessages)
	{
		if (runSelect)
		{
			//Select, Cluster, Combine
			if (select())
			{
				DEBUG("Select OK.");
				cluster(); //Find the system peer and the selection jitter
				combine(); //Calculate the final offset and jitter
				DEBUG("Combining done.");
				//Sync clock
				syncTime();

				if (systemStatLogEnabled)
				{
					unsigned long RunTime = KernelInterface::getInstance().getProcessTimeMS() / 1000;
					spdlog::get("systemstat")->info("{}\t{:.6f}\t{:.6f}\t{:.6f}\t{:.6f}", RunTime, systemOffset * 1000, systemJitter * 1000, selectionJitter * 1000, kernelInterface.getFrequencyOffset());
				}
			}

			//Update system variables for other process
			kernelInterface.updateSystemVariables(systemVariables);
		}
		else
		{
			DEBUG("No new information available, no need to run system process.");
		}
	}

	//Sleep until next poll
	//This must change if we decide to implement burst mode.
	//Implement in a way that it sleeps exactly until the next Poll?
	// std::this_thread::sleep_until(lastPoll + std::chrono::seconds(1));
	std::this_thread::sleep_for(std::chrono::seconds(1));
}

/**
 *
 * @brief	This implements the Selection Algorithm as defined by RFC 5905 in section 11.2.1.
 *
 * @return	Returns whether an intersection intervall has been found (step 5A, True) or not (step 5B, False).
 * 			If the return-value was false, the cluster and combine algorithm need not be
 * 			called and the system clock will not be synchronized.
 *
 *
 */

bool Synchronizer::select()
{
	bool result = true;
	int numberOfPeers = 0; //=m
	int falsetickers = 0;  //=f
	int midpoints = 0;     //=d
	int lowerIndex = 0;    //=l
	int upperIndex = 0;    //=u
	int c = 0;             //=c
	bool stop = false;
	bool gotoStepThree = false; //For looping back after step 5B.

	//Clear the candidate list
	candidateList.clear();

	//Step 1
	//Generate three tuples for each Peer that is usable
	//Maybe use reach-register in getPeer method to skip certain Peers
	std::for_each(peerList.begin(), peerList.end(), [this, &numberOfPeers](Peer* peer) {
		if (peer->isAcceptable()) //If the peer is acceptable for synchronization
		{
			numberOfPeers++;
			candidateList.push_back(ntp::SelectionTuple(peer, 1));
			candidateList.push_back(ntp::SelectionTuple(peer, 0));
			candidateList.push_back(ntp::SelectionTuple(peer, -1));
		}
	});

	//Are there candidates?
	if (candidateList.size() <= 0)
	{
		result = false;
		DEBUG("No acceptable synchronization candidates.");
	}
	else
	{
		//Step 2
		//Sort items by edge
		std::sort(candidateList.begin(), candidateList.end());

		do //To loop back from step 5B
		{
			//Step 3
			//Scan from lowest endpoint to highest. Add one to c for every lowpoint,
			//subtract one for every highpoint, add one to d for every midpoint.
			//If c >= m-f, stop; set l = current lowpoint
			stop = false;
			for (unsigned int i = 0; i < candidateList.size() && !stop; i++)
			{
				c -= candidateList.at(i).getType();

				if (candidateList.at(i).getType() == SYNC_MIDPOINT)
				{
					midpoints++;
				}

				if (c >= numberOfPeers - falsetickers) //If c >= m - f
				{
					lowerIndex = i;
					CHATTY_POLL_PROCESS("Found lower index: {}", lowerIndex);
					stop = true;
				}
			}

			c = 0;
			//Scan from highest endpoint to lowest. Add one to c for every
			//highpoint, subtract one for every lowpoint, add one to d
			//for every midpoint. If c >= m-f, stop; set u = current highpoint

			stop = false;
			for (int i = candidateList.size() - 1; i >= 0 && !stop; i--)
			{
				c += candidateList.at(i).getType();

				if (candidateList.at(i).getType() == SYNC_MIDPOINT)
				{
					midpoints++;
				}

				if (c >= numberOfPeers - falsetickers) //If c >= m - f
				{
					upperIndex = i;
					CHATTY_POLL_PROCESS("Found upper index: {}", upperIndex);
					stop = true;
				}
			}

			//5. Is d= f and l < u? If yes, then follow step 5A; else follow step 5B
			if (midpoints == falsetickers && lowerIndex < upperIndex)
			{
				//Step 5A: Success, the intersection interval is [lower, upper]
				gotoStepThree = false;
				//Check if the number of truechimers (numberOfPeers - falsetickers)
				//is greater or equal to CMIN.

				if (numberOfPeers - falsetickers >= MIN_CANDIDATES)
				{
					//Success! We can continue with clustering.
					result = true;
				}
				else
				{
					//Failure, not enough valid candidates
					result = false;
				}
			}
			else
			{
				//Step 5B: Add one to f. Is f < (m / 2)? If yes, then go to step 3 again.
				//If no then go to step 6.
				falsetickers++;
				double halfPeers = 0.5 * numberOfPeers;
				if (falsetickers < halfPeers)
				{
					gotoStepThree = true;
				}
				else
				{
					//Step 6: Majority clique not found,
					//no suitable candidates to discipline the system clock.
					gotoStepThree = false;
					result = false;
					INFO("Select Algorithm: No suitable candidates to discipline the system clock.");
				}
			}

		} while (gotoStepThree); //Maybe loop to step 3 after step 5B.
	}

	if (result)
	{
		//If we found a majority clique, place the Peers on the survivor List as
		//preparation for the cluster-Algorithm
		survivorList.clear();
		//For the midpoints in the intervall from lower to upper, put the Peers on the List.
		for (int i = lowerIndex; i < upperIndex; i++)
		{
			if (candidateList.at(i).getType() == SYNC_MIDPOINT)
			{
				survivorList.push_back(ntp::SurvivorCandidate(candidateList[i].getPeer()));
			}
		}
	}

	return result;
}

/**
 *
 * @brief	This implements the Cluster Algorithm as specified in Section
 * 			11.2.2 of RFC 5905 to select a System Peer.
 * 			This function requires that the selection algorithm has been
 * 			run successfully and the Peers of the majority clique have been
 * 			placed on the survivorList.
 *
 *
 */

void Synchronizer::cluster()
{
	double selectionJitter = 0;
	double psi_min = 2e9;
	double psi_max = -2e9;
	int maxCand; //Index of the candidate that had maximum psi in the last round
	bool continueLoop = true;

	//Step 2: Sort the candidates by increasing lambda_p (merit factor).
	// Let n be the number of candidates and NMIN the minimum required number of survivors.
	std::sort(survivorList.begin(), survivorList.end());

	while (continueLoop)
	{
		//3.  For each candidate, compute the selection jitter psi_s:
		//
		//			+-----						   -----+^1/2
		//			|		 n-1 						|
		//			|		 --- 				 		|
		//			|	1	 \ 					   2	|
		//psi_s = 	| ---- * /  (theta_s - theta_j) 	|
		//			|  n-1   --- 						|
		//			|		 j=1 						|
		//			+----- 						   -----+
		//3. Select psi_max as the candidate with maximum psi_s.
		//4. Select psi_min as the candidate with minimum psi_p. (jitter)

		for (unsigned int i = 0; i < survivorList.size(); i++) //For each candidate
		{

			//Calculate selectionJitter
			selectionJitter = 0;
			//Counting like this is possible since the difference of Offsets with oneself
			//does not contribute to the sum.
			for (unsigned int j = 0; j < survivorList.size(); j++)
			{
				selectionJitter += pow((survivorList[i].getOffset() - survivorList[j].getOffset()), 2);
			}
			selectionJitter = sqrt(selectionJitter / (survivorList.size() - 1));

			//Step 3
			if (selectionJitter > psi_max)
			{
				psi_max = selectionJitter;
				maxCand = i;
			}

			//Step 4
			if (survivorList[i].getJitter() < psi_min)
			{
				psi_min = survivorList[i].getJitter();
			}
		}

		//Step 6
		if (psi_max < psi_min || survivorList.size() <= static_cast<unsigned int>(abs(MIN_SURVIVORS)))
		{
			//Step 6A
			continueLoop = false;
			//Done! The First candidate on the survivor List is the System Peer!
			DEBUG("Clustering done.");
		}
		else
		{
			//Step 6B
			continueLoop = true;
			survivorList.erase(survivorList.begin() + maxCand);

			//selectionJitter is the system selection jitter PSI_s
		}

	} //End of while
}

/**
 * @brief	This implements the Combine Algorithm as specified in
 * 			section 11.2.3 of RFC 5905 to calculate the system offset and jitter.
 * 			Also refer to section A.5.5.5.
 *
 */

void Synchronizer::combine()
{
	//### Combine Algorithm ###//

	//Calculate the weighted averages of Jitter and Offset from the Survivors.
	//They are weighted by the reciprocal root synchronization distance.
	double avgJitter = 0;
	double avgOffset = 0;
	double tempRootSyncDist = 0;
	double divider = 0;

	for (unsigned int i = 0; i < survivorList.size(); i++)
	{
		tempRootSyncDist = survivorList[i].getPeer()->getSyncDistance();
		if (tempRootSyncDist != 0)
		{
			divider += 1 / tempRootSyncDist;
			avgOffset += survivorList[i].getOffset() / tempRootSyncDist;
			avgJitter += survivorList[i].getJitter() / tempRootSyncDist;
		}
	}

	if (divider != 0)
	{
		//Calculate the system Jitter (Combine Algorithm)
		//PSI_s ist selectionJitter
		avgJitter = avgJitter / divider;
		systemJitter = sqrt(pow(selectionJitter, 2) + pow(avgJitter, 2));

		//Calculate the system offset as a weighted average of the survivor offsets.
		systemOffset = avgOffset / divider;
	}
}

/**
 * @brief	Use the computed offset to discipline the clock. This basically
 * 			implements the clock_update-Routine as defined in sections 11.2.3
 * 			and A.5.5.4. of RFC 5905.
 *
 * @throws	Throws TimeExceptions when it can't adjust or sync the time, or when the offset exceeds the panic threshold.
 */

void Synchronizer::syncTime()
{
	Constants& constants = Constants::getInstance();

	//We mainly need to decide whether we have to step the system clock,
	//slew it or panic since the offset is way too large.

	//Determine whether the update is even a new update
	//This is checked against the soon-to-be system peer, which is still at first place
	//in the survivorList
	ntp::Peer* systemPeer = survivorList[0].getPeer();
	double timediff = kernelInterface.calcTimeDifference(systemPeer->getLastReceived(), lastSync);

	if (timediff > 0)
	{
		//Yes, this is a new update
		lastSync = systemPeer->getLastReceived();
		synchronized = true;

		//Do we need to panic?
		if (PANIC_ENABLED && abs(systemOffset) > PANIC_THRESHOLD)
		{
			LOG_ERROR("Offset is greater than the Panic-Threshold. Exiting.");
			throw TimeException("Offset is greater than the Panic-Threshold.");
		}
		else if (abs(systemOffset) > STEP_THRESHOLD) //Do we need to step?
		{
			//Step the time
			INFO("STEPPING TIME -> Offset [s]: {}", systemOffset);
			try
			{
				kernelInterface.setTime(systemOffset);
			}
			catch (ntp::TimeException& ex)
			{
				LOG_ERROR("Could not set time.");
				throw ex;
			}

			//All peers need to be reset
			std::for_each(peerList.begin(), peerList.end(), [this](Peer* peer) {
				peer->reset();
			});

			//In the reference implementation, s.stratum and s.poll are set, stratum to maxstrat and poll to minpoll
			systemVariables.setStratum(constants.getUChar(ntp::ConstantsUChar::maxStrat));
			systemVariables.setPoll(constants.getUChar(ntp::ConstantsUChar::pollMin));
			jiggleCount = 0;
		}
		else //Seems like we can just slew
		{
			//In RFC 5905, p.96:

			//		x	s.leap = p->leap;										-> switch leap indicator
			//		x	s.stratum = p->stratum + 1;								-> update stratum
			//		x	s.refid = p->refid;										-> update reference id
			//			s.reftime = p->reftime;									???? why?
			//		x	s.rootdelay = p->rootdelay + p->delay;					-> update rootdelay
			//			dtemp = SQRT(SQUARE(p->jitter) + SQUARE(s.jitter));
			//			dtemp += max(p->disp + PHI * (c.t - p->t) +
			//			fabs(p->offset), MINDISP);
			//		x	s.rootdisp = p->rootdisp + dtemp;						-> update rootdispersion

			systemVariables.setStratum(systemPeer->getStratum() + 1);

			//Set the ReferenceID
			//Reference ID is the address of the systemPeer
			//As client, our stratum can't be <= 1.

			//From RFC 5905, p. 22
			//Above stratum 1 (secondary servers and clients): this is the
			//reference identifier of the server and can be used to detect timing
			//loops. If using the IPv4 address family, the identifier is the fouroctet IPv4 address.
			//If using the IPv6 address family, it is the
			//first four octets of the MD5 hash of the IPv6 address.

			char refID[4];
			IPAddress systemPeerIP = systemPeer->getAddress();
			uint32_t IP = systemPeerIP.getAddress();
			for (int i = 0; i < 4; i++)
			{
				refID[i] = ((char*)&IP)[i];
			}
			systemVariables.setReferenceID(refID);

			switch (systemPeer->getLeapIndicator())
			{
			case LI_NO_WARNING:
				kernelInterface.resetLeapSecond();
				break;
			case LI_61SEC:
				kernelInterface.insertLeapSecond();
				break;
			case LI_59SEC:
				kernelInterface.deleteLeapSecond();
				break;
			default:
				break;
			}

			systemVariables.setLeapIndicator(systemPeer->getLeapIndicator());
			systemVariables.setRootDelay(systemPeer->getRootDelay() + systemPeer->getDelay());

			//Set Reference Timestamp
			systemVariables.setReferenceTimestamp(kernelInterface.getTime());

			//Calculate Root Dispersion
			double temp = sqrt(pow(systemPeer->getJitter(), 2) + pow(systemJitter, 2));
			double temp2 = systemPeer->getDispersion() + constants.getDouble(ntp::ConstantsDouble::phi) * kernelInterface.calcTimeDifference(lastSync, systemPeer->getLastReceived()) + fabs(systemPeer->getOffset());
			if (constants.getDouble(ntp::ConstantsDouble::minDisp) > temp2)
			{
				temp2 = constants.getDouble(ntp::ConstantsDouble::minDisp);
			}
			systemVariables.setRootDispersion(static_cast<double>(systemPeer->getRootDispersion()) + temp + temp2);

			//Adjust the poll intervall (system variable)
			//See RFC5905, p. 102
			if (fabs(systemOffset) < SYNC_POLL_GATE * kernelInterface.getJitter())
			{
				jiggleCount += systemVariables.getPoll();
				if (jiggleCount > SYNC_POLL_LIMIT)
				{
					jiggleCount = SYNC_POLL_LIMIT;
					if (systemVariables.getPoll() < constants.getUChar(ntp::ConstantsUChar::pollMax))
					{
						jiggleCount = 0;
						systemVariables.increasePoll();
					}
				}
			}
			else
			{
				jiggleCount -= systemVariables.getPoll() << 1;
				if (jiggleCount < (SYNC_POLL_LIMIT * -1))
				{
					jiggleCount = SYNC_POLL_LIMIT * -1;
					if (systemVariables.getPoll() > constants.getUChar(ntp::ConstantsUChar::pollMax))
					{
						jiggleCount = 0;
						systemVariables.decreasePoll();
					}
				}
			}

			kernelInterface.updateSystemVariables(systemVariables);

			//Adjust the time
			INFO("ADJUSTING TIME -> Offset [s]: {: .6f}", systemOffset);

			try
			{
				kernelInterface.adjustTime(systemOffset);
			}
			catch (ntp::TimeException& ex)
			{
				LOG_ERROR("Could not adjust time.");
				throw TimeException("Could not adjust the system-time.");
			}
		}

	} // End of the update-time-check
	else
	{
		DEBUG("Not syncing the clock - Old update.");
	}
}

ntp::LongTimestamp Synchronizer::getReferenceTimestamp()
{
	return systemVariables.getReferenceTimestamp();
}

double Synchronizer::getSystemJitter()
{
	return systemJitter;
}

/**
 * @brief	Searches the peerList for a Peer with the corresponding address. Returns true on success.
 *
 * Example Usage:
 * @code
 * 		sockaddr_in address;	//Address to search for
 * 		ntp::Peer * peer;		//Pointer to ntp::Peer that is meant to be filled in
 *
 * 		if(getPeerByAddress(address, &peer))
 * 		{
 * 			//Peer was found
 * 			//Do something with Peer:
 * 			peer->triedReach();
 * 		}
 * 		else
 * 		{
 * 			//Peer was not found
 * 		}
 * @endcode
 *
 * @param [in] peerAddress The address of the peer.
 * @param [out] peer If the return value is true, than this is a pointer to a pointer to the found peer.
 *
 * @return Returns true if a Peer was found, and false if no Peer was found.
 *
 * @remarks	This can be changed to a binary search or other search algorithms if desired
 * 			since the list is sorted anyway. However it is not important since the list
 * 			will probably not get very long.
 */

bool Synchronizer::getPeerByAddress(IPAddress peerAddress, ntp::Peer** peer)
{
	bool success = false;
	uint32_t findIP = peerAddress.getAddress();

	for (auto const& currentPeer : peerList)
	{
		if (currentPeer->getAddress().getAddress() == findIP)
		{
			success = true;
			*peer = currentPeer;
			break;
		}
	}
	// If no Peer was found, false is returned

	return success;
}

/**
 * @brief	loads peers from the config-file and adds them to the peerList
 * @details	It looks for the key "Peers" in section "ClientMode" in the
 * 			ConfigFile. The key should contain hostnames or IP-addresses of the Peers separated by spaces.
 *
 * @returns	Returns true if peers where added to the list, and false if there were no valid peers.
 *
 * @remarks Hostnames are resolved by a DNS-request throught the KernelInterface. However it is not
 * 			clear if pure IP-addresses are checked by the KernelInterface-function. So for right now,
 * 			if a address is input that does not exist, the client will not check if the server actually exists
 * 			put will only notice a non-existent server when no packets return from it.
 */
bool Synchronizer::loadPeersFromConfig()
{
	DEBUG("Loading Peers from Config.");
	ConfigFile& config = ConfigFile::getInstance();
	std::string peersConfig = "";
	bool ret = false;

	if (!config.readString(peersConfig, "ClientMode", "Peers"))
	{
		LOG_ERROR("Attempt to load peer list from configfile failed!");
		return false;
	}

	//Split the peersConfig-string into several addresses

	std::stringstream configStream(peersConfig);
	std::string temp = "";
	std::vector<std::string> peerAddresses;

	while (getline(configStream, temp, ' '))
	{
		peerAddresses.push_back(temp);
		DEBUG("Getting Peer Address: {}", temp);
	}

	if (peerAddresses.size() < 1)
	{
		LOG_ERROR("There are no Peer-addresses in the config file.");
		return false;
	}

	INFO("Looking up the Peer-addresses. If you're not connected to the internet, this will fail after a while.");
	for (auto const& peerAddress : peerAddresses)
	{
		if (!addPeerByHostname(peerAddress))
		{
			LOG_ERROR("Could not add the peer with the hostname / address: {}", peerAddress);
		}
		else
		{
			ret = true;
		}
	}

	return ret;
}

/**
 * @brief	Creates and adds a new Peer to the peerList.
 * @details The new element is added at a certain position so that the
 * 			list stays sorted after the address. The first item is the lowest
 * 			address.
 *
 * 	@param [in] peerAddress The IP-Address of the new Peer to be created.
 */
Peer& Synchronizer::addPeerByAddress(IPAddress peerAddress)
{
	unsigned int newIP = peerAddress.getAddress();
	Peer* newPeer = new Peer(peerAddress);
	unsigned int currentSize = peerList.size();

	if (peerList.size() > 0)
	{
		for (unsigned int i = 0; i < currentSize; i++) //Iterate through vector
		{
			if (peerList.at(i)->getAddress().getAddress() > newIP)
			{
				peerList.insert(peerList.begin() + i, newPeer); //Insert at right spot
				break;
			}
			else if (i == currentSize - 1)
			{
				peerList.push_back(newPeer);
			}
		}
	}
	else
	{
		peerList.push_back(newPeer);
	}

	INFO("A new Peer with address {} was added to the PeerList.", peerAddress.toString());

	return *newPeer;
}

/**
 * @brief	Creates and adds a new Peer to the PeerList.
 * @details The new element is added at a certain position so that the
 * 			list stays sorted by address. The first item is the lowest
 * 			address.
 *
 * 	@param [in] hostname The hostname of the new Peer to be created.
 *
 * 	@return	Returns false if the hostname could not be resolved.
 */
bool Synchronizer::addPeerByHostname(std::string hostname)
{
	//This call might throw a ntp::NetworkException
	IPAddress address;
	try
	{
		address = kernelInterface.getAddressFromName(hostname);
	}
	catch (NetworkException& ex)
	{
		LOG_ERROR(ex.toString());
		return false;
	}

	Peer& newPeer = addPeerByAddress(address);
	newPeer.setHostname(hostname);

	return true;
}

/**
 * @brief	Calculates the root synchronization distance of the peer. Refer to the root_dist routine in A.5.5.2. of RFC 5905.
 *
 * @param 	peer	The Peer to calculate the distance for.
 */

double Synchronizer::calcRootSyncDistance(Peer& peer)
{
	double rootSyncDist = 0;
	double totalDelay = peer.getRootDelay() + peer.getDelay();
	double minDisp = Constants::getInstance().getDouble(ntp::ConstantsDouble::minDisp);
	if (minDisp > totalDelay)
	{
		totalDelay = minDisp;
	}

	rootSyncDist = totalDelay / 2.0 + peer.getRootDispersion() + peer.getDispersion() + Constants::getInstance().getDouble(ntp::ConstantsDouble::phi) * kernelInterface.calcTimeDifference(lastSync, peer.getLastReceived()) + peer.getJitter();

	//DEBUG("Peer " + peer.getHostname() + ": RootSyncDist = " + std::to_string(rootSyncDist));
	return rootSyncDist;
}

/**
 * @brief	Returns whether the client is synchronized (i.e. the time has been set at least once) or not.
 *
 *
 */
bool Synchronizer::isSynchronized()
{
	return synchronized;
}

/**
 * @brief	Used to toggle whether the system time is adjusted or not. Does not affect the sending of packets.
 *
 * @param [in] state True enables the setting and adjusting of the system time, false disables it.
 *
 */
void Synchronizer::setSync(bool state)
{
	syncEnabled = state;
}

} //End of namespace ntp
