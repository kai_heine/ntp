/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

/**
 * @file 	Receiver.hpp
 * @details This Header declares the NTP Receiver Thread.
 * @date 	01.05.2016
 * @author 	silvan
 * @version	1
 */

#ifndef NTP_THREAD_RECEIVER_HPP_
#define NTP_THREAD_RECEIVER_HPP_

#include "../Datatypes/Frame/Message.hpp"
//#include "../NtsInterface.hpp"
#include "../Utility/KernelInterface.hpp"
#include "../Utility/target_specific_mutex.hpp"
#include "Transmitter.hpp"
#include <NtsUnicastService.h>
#include <memory>
#include <queue>
#include <stdexcept>

/// minimum version number
/// @remarks: Load version numbers from configuration file?
#define MIN_NTP_VERSION 4

/// maximum version number
/// @remarks: Load version numbers from configuration file.
#define MAX_NTP_VERSION 4

/// maximum amount of allowed packets in queue
#define MAX_PACKETS_IN_QUEUE 50

namespace ntp
{

/**
 * @brief	This class implements the receive process.
 * @details	A receiver object is neither copy-constructable
 * 			nor copy-movable. It is intended to be constructed once
 * 			in a program and should run until the program ends.
 *
 * 			It waits for incoming packets, validates them, and finally
 * 			adds them to the receiveQueue. Other processes my access the queue
 * 			using the getOldestMessage() method. The integrated queue is
 * 			thread-safe. A mutex will guarantee that no data races happen
 * 			when trying to access the queue from different threads.
 */
class Receiver
{
private:
	class TooManyPacketsInQueue : std::exception
	{
		const char* what() const noexcept;
	};

private:
	std::queue<Message> receiveQueue;
	std::mutex receiverMutex;
	Transmitter* transmitter;

public:
	Receiver(Transmitter& transmitter);
	Receiver(const Receiver& r) = delete;
	~Receiver();
	Message getOldestMessage(void);
	bool isMessageAvailable(void);
	void operator()();
	Receiver& operator=(const Receiver& r) = delete;

private:
	bool validatePacket(std::shared_ptr<Message> msg) const;
	void addMsgToQueue(const Message& msg);
	void receiveProcessClient(void);
	void receiveProcessServer(void);
};

} // end of namespace ntp

#endif /* NTP_THREAD_RECEIVER_HPP_ */
