/**
 * @file 	NTP.cpp
 * @details This File implements the NTP Class, which is the entrance point to the program
 * @date 	11.05.2016
 * @author 	Christian Jütte
 * @version	0.1
 *
 * @remarks Just added support for "getMode" for development of the threads.
 * 			The structure of this class is still subject to change.
 */

#include "NTP.hpp"
#include "Log/Logger.hpp"
#include "Thread/ConsoleInterface.hpp"
#include "Thread/Receiver.hpp"
#include "Thread/Synchronizer.hpp"
#include "Thread/Transmitter.hpp"
#include "Utility/ConfigFile.hpp"
#include "Utility/Constants.hpp"
#include "Utility/KernelInterface.hpp"
#include "Utility/Version.hpp"
#include <exception>

ntp::ConfigFile ntp::ConfigFile::instance("config/ntp/config.ini");
ntp::Constants ntp::Constants::instance;
ntp::KernelInterface ntp::KernelInterface::instance;
NtsUnicastService ntp::NTP::nts;

namespace ntp
{

NTP::AppMode NTP::mode = NTP::AppMode::MODE_SERVER;
std::exception_ptr NTP::exceptionPtr;

/**
 * @brief	Constructor of NTP
 * @details	Initializes the NTP class.
 */
NTP::NTP()
{
	mode = NTP::AppMode::MODE_SERVER;

	ConfigFile& config = ConfigFile::getInstance();

	if (!config.existConfigFile())
	{
		throw std::runtime_error("Can't access the config-file " + config.getConfigFile());
	}

	if (!config.isConfigFileValid())
	{
		throw std::runtime_error("The config-file " + config.getConfigFile() + " uses an invalid encoding. Only ANSI is supported currently.");
	}

	{
		bool fileLogging = false;
		config.readBool(fileLogging, "GeneralSettings", "FileLogging", false);

		bool consoleLogging = true;
		config.readBool(consoleLogging, "GeneralSettings", "ConsoleLogging", true);

		std::string loggingDir;
		if (fileLogging)
		{
			config.readString(loggingDir, "GeneralSettings", "LogDirectory", "log");
		}

		std::string logLevel = "info";
		config.readString(logLevel, "GeneralSettings", "LoggingMode", "info");

		logInit(consoleLogging, fileLogging, logLevel, loggingDir);
	}

	setAppModeFromConfig(config);

	if (!KernelInterface::programHasNecessaryRights())
	{
		throw std::runtime_error("The program was not run with the necessary rights. Exiting!");
	}

#ifdef _DEBUG
	INFO("NTPv4 (rfc 5905, NTPv3 not supported) v{} ({}) - debug build", NTP_VERSION, NTP_COMPILE_TIME);
#else
	INFO("NTPv4 (rfc 5905, NTPv3 not supported) v{} ({}) - release build", NTP_VERSION, NTP_COMPILE_TIME);
#endif
	INFO(std::string("NTP started, NTP Mode: ") +
	     (mode == NTP::AppMode::MODE_SERVER ? "Server" : "Client"));

	KernelInterface& ki = KernelInterface::getInstance();

	//First, check if another process of the same kind is already running.
	//This also initializes shared memory
	if (ki.isOtherProcessRunning())
	{
		//Get mode
		std::string modeStr;
		switch (mode)
		{
		case MODE_CLIENT:
			modeStr = "Client";
			break;
		case MODE_SERVER:
			modeStr = "Server";
			break;
		}
		std::string error = "Another NTP-";
		error.append(modeStr);
		error.append(" is already running. Exiting the application.");
		LOG_ERROR(error);
		throw(ntp::FileException(error, true));
	}

	//Initialize the system variables to default values
	ki.updateSystemVariables(SharedSystemVariables());

	// initialize connection
	ki.initSocket();

	bool useNTS = false;
	if (config.readBool(useNTS, "ClientMode", "UseNTS", false))
	{
		DEBUG(std::string("config: UseNTS successfully read: UseNTS=") + std::to_string(useNTS));
	}
	else
	{
		WARN("UseNTS not found in config. Not using NTS.");
	}

	// use NTS in server-mode or if wanted in client-mode
	if (getMode() == MODE_SERVER || (getMode() == MODE_CLIENT && useNTS))
	{
		try
		{
			// NTS Unicast
			nts.initialize("./config/nts/global.ini");
			nts.showInfos();
		}
		catch (std::string const& s)
		{
			LOG_ERROR(s);
		}
		catch (std::exception& e)
		{
			LOG_ERROR("nts exception: {}", e.what());
		}
		catch (...)
		{
			throw std::runtime_error("An unknown exception occured during nts initialization");
		}
	}
}

/**
 * @brief   NTP::run
 * @details Runs the NTP service in either client or server mode, depending on the configuration.
 * @see		Transmitter.cpp, fastTransmit(ntp::Message msg)
 */
void NTP::run()
{
	bool consoleUserIO = false;
	ConfigFile::getInstance().readBool(consoleUserIO, "GeneralSettings", "ConsoleUserIO", false);

	// start program depending on mode
	switch (mode)
	{
	case MODE_CLIENT:
	{
		Transmitter transmThread;
		Receiver recThread(transmThread);
		Synchronizer syncThread(recThread, transmThread);

		std::thread t1(std::ref(transmThread));
		std::thread t2(std::ref(recThread));
		std::thread t3(std::ref(syncThread));

		// Start thread for console I/O
		if (consoleUserIO)
		{
			ConsoleInterface ioThread(recThread, transmThread, syncThread);
			std::thread t4(std::ref(ioThread));
			t4.join();
		}

		t1.join();
		t2.join();
		t3.join();

		std::rethrow_exception(exceptionPtr);
		break;
	} // end of case Client

	case MODE_SERVER:
	{
		Transmitter transmitter;
		Receiver recThread(transmitter);
		ConsoleInterface ioThread(recThread);

		std::thread t1(std::ref(recThread));

		if (consoleUserIO)
		{
			std::thread t2(std::ref(ioThread)); // Start thread for console I/O
			t2.join();
		}

		t1.join();

		std::rethrow_exception(exceptionPtr);
		break;
	}
	}
}

/**
 * @brief Getter method, gives the mode of the application.
 * @details With this method, you can get information about the mode of the application (Client, Server).
 */
NTP::AppMode NTP::getMode()
{
	return mode;
}

/**
 * @brief	sets the exception pointer
 *
 */
void NTP::setExceptionPtr(std::exception_ptr exceptionPtr)
{
	NTP::exceptionPtr = exceptionPtr;
}

/**
 * @brief	returns the exception_ptr
 *
 */
std::exception_ptr NTP::getExceptionPtr()
{
	return NTP::exceptionPtr;
}

/**
 * @brief	returns the instance of NtsUnicastService to use with NtsInterface
 *
 */
NtsUnicastService& NTP::getNtsUnicastService()
{
	return NTP::nts;
}

/**
 * @brief 	sets the AppMode depending on the given configuration
 * @details	It reads the configured mode from the key "AppMode" in section "GeneralSettings"
 * 			in the ConfigFile.
 * 			If the AppMode is not set in the configuration, this method will cause the
 * 			application to exit.
 */
void NTP::setAppModeFromConfig(ConfigFile& config)
{
	std::string configAppMode;

	if (!config.readString(configAppMode, "GeneralSettings", "AppMode", ""))
	{
		LOG_ERROR("Failed to read AppMode from ConfigFile!");
	}

	if (configAppMode == "")
	{
		LOG_ERROR("Failed to read AppMode from ConfigFile!");
		exit(1);
	}
	else if (configAppMode == "CLIENT")
	{
		DEBUG("Loaded from config: program running as client");
		mode = NTP::AppMode::MODE_CLIENT;
	}
	else if (configAppMode == "SERVER")
	{
		DEBUG("Loaded from config: program running as server");
		mode = NTP::AppMode::MODE_SERVER;
	}
}

} //End of namespace ntp
