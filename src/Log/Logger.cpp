/**
 * @file 	Logger.cpp
 * @details Simple Logging API
 * @date 	08.07.2016
 * @author 	silvan
 * @version	0.2
 */

#include "Logger.hpp"
#include <map>
#include <spdlog/spdlog.h>

#include <spdlog/async.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include <boost/filesystem/operations.hpp>

namespace ntp
{

void logInit(bool consoleLogging, bool fileLogging, std::string logLevel, std::string const& logDirectory)
{
	std::vector<spdlog::sink_ptr> sinks;
	if (consoleLogging)
	{
		sinks.push_back(std::make_shared<spdlog::sinks::stdout_color_sink_mt>());
	}
	if (fileLogging)
	{
		boost::filesystem::create_directories(logDirectory);
		sinks.push_back(std::make_shared<spdlog::sinks::rotating_file_sink_mt>(
		    logDirectory + "/ntp.log", 1024 * 1024, 10));
	}

	// logger creation
	spdlog::init_thread_pool(8192, 1);
	auto log = std::make_shared<spdlog::async_logger>(
	    "ntp", begin(sinks), end(sinks), spdlog::thread_pool());
	spdlog::register_logger(log);

	// log level
	std::transform(begin(logLevel), end(logLevel), begin(logLevel), [](char c) { return std::tolower(c); });
	auto level = spdlog::level::from_str(logLevel);
	if (level == spdlog::level::off)
	{
		level = spdlog::level::info;
	}

	log->set_level(level);
	log->set_pattern("%T  [%^%l%$] [%n]  %v");
}

spdlog::logger* logger()
{
	static auto logger = spdlog::get("ntp");
	return logger.get();
}

std::shared_ptr<spdlog::logger> createFileLogger(std::string name, std::string path, std::string header)
{
	namespace fs = boost::filesystem;

	fs::create_directories(fs::path{path}.parent_path());

	// rename old files (do manually until spdlog is updated to rotate on open)
	if (fs::exists(path))
	{
		fs::rename(path, path + ".old");
	}

	// make sure first line is logged immediately without any formatting
	auto syncLogger = spdlog::create<spdlog::sinks::rotating_file_sink_mt>(name, path, 1024 * 1024, 10);
	syncLogger->set_pattern("%v");
	syncLogger->info("Date\tLocal Time\t" + header);
	spdlog::drop(name);

	auto fileLogger = spdlog::create_async<spdlog::sinks::rotating_file_sink_mt>(name, path, 1024 * 1024, 10);
	fileLogger->set_pattern("%Y-%m-%d\t%H:%M:%S\t%v");
	return fileLogger;
}

} // namespace ntp
