/* 
 * ----------------------------------------------------------------------------
 * Copyright 2018 Ostfalia University of Applied Sciences, Germany
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ----------------------------------------------------------------------------
 */

#ifndef NTP_LOG_LOGGER_HPP_
#define NTP_LOG_LOGGER_HPP_

#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE

#include <spdlog/spdlog.h>

namespace ntp
{
void logInit(bool consoleLogging = true, bool fileLogging = false, std::string logLevel = "info", std::string const& logDirectory = "log");
spdlog::logger* logger();
std::shared_ptr<spdlog::logger> createFileLogger(std::string name, std::string path, std::string header);
} // namespace ntp

#define LOG_ERROR(...) SPDLOG_LOGGER_ERROR(ntp::logger(), __VA_ARGS__)
#define WARN(...) SPDLOG_LOGGER_WARN(ntp::logger(), __VA_ARGS__)
#define INFO(...) SPDLOG_LOGGER_INFO(ntp::logger(), __VA_ARGS__)
#define DEBUG(...) SPDLOG_LOGGER_DEBUG(ntp::logger(), __VA_ARGS__)
#define CHATTY_POLL_PROCESS(...) SPDLOG_LOGGER_TRACE(ntp::logger(), __VA_ARGS__)
#define CHATTY_UPDATE_SYSTEM_VARIABLES(...) SPDLOG_LOGGER_TRACE(ntp::logger(), __VA_ARGS__)
#define DEFAULT_LOG(...) SPDLOG_DEBUG(__VA_ARGS__)

#endif /* NTP_LOG_LOGGER_HPP_ */
