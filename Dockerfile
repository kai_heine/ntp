# build native image by default
# change with --build-arg ARCH=armhf
ARG ARCH=native

#native base
FROM alpine:3.9 as native

# armhf base
FROM arm32v7/alpine:3.9 as armhf
ADD qemu-arm-static.tar.gz /usr/bin


# build stage
FROM ${ARCH} as builder
RUN apk add --no-cache g++ ninja cmake openssl-dev boost-dev boost-static

WORKDIR /ntp/

COPY src/ /usr/src/ntp/src/
COPY nts/ /usr/src/ntp/nts/
COPY CMakeLists.txt /usr/src/ntp/

RUN cmake -G Ninja -DCMAKE_BUILD_TYPE=MinSizeRel -DNTP_MUSL_SUPPORT=ON /usr/src/ntp
RUN cmake --build . --target ntp

# final image
FROM ${ARCH}

WORKDIR /ntp/

COPY --from=builder /ntp/src/ntp /usr/bin/
COPY ./bin .
RUN apk add --no-cache libssl1.1 libstdc++


CMD ["/usr/bin/ntp"]
